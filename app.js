const express = require('express');
const bodyParser = require('body-parser');

const checkAuth = require('./middleware/CheckAuth');
const financeRoute = require('./routes/finance');
const stockinRoute = require('./routes/stockin');
const stockoutRoute = require('./routes/stockout');
const rolesRoute = require('./routes/roles');
const usersRoute = require('./routes/users');
const spinformationRoute = require('./routes/spinformation');
const ordersRoute = require('./routes/orders');
const financePriceRoute = require('./routes/financePrice');
const loginRoute = require('./routes/login');
const warehouseRoute = require('./routes/warehouse');

const app = express();
app.use(bodyParser.json());
app.use(
  bodyParser.urlencoded({
    extended: true,
  }),
);

app.use('/login', loginRoute);
app.use('/finance', checkAuth, financeRoute);
app.use('/stockin', checkAuth, stockinRoute);
app.use('/stockout', checkAuth, stockoutRoute);
app.use('/roles', checkAuth, rolesRoute);
app.use('/users', checkAuth, usersRoute);
app.use('/spinformation', checkAuth, spinformationRoute);
app.use('/orders', checkAuth, ordersRoute);
app.use('/financePrice', checkAuth, financePriceRoute);
app.use('/warehouse', checkAuth, warehouseRoute);

module.exports = app;
