const http = require('http');
const port = process.env.PORT || 5000;
const app = require('./app');
const mongoose = require('mongoose');

mongoose.set('useCreateIndex', true);
mongoose.connect(
  'mongodb://admin:' +
    process.env.MONGO_PASS +
    '@cluster0-shard-00-00-of1fd.mongodb.net:27017,cluster0-shard-00-01-of1fd.mongodb.net:27017,cluster0-shard-00-02-of1fd.mongodb.net:27017/reactpttlogi?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin&retryWrites=true',
  { useNewUrlParser: true },
);

require('./services/calculateStore')();
const server = http.createServer(app);
server.listen(port, () => {
  console.log('listening at ' + port);
});
