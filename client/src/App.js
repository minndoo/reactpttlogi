import React, { Component } from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { library } from '@fortawesome/fontawesome-svg-core';
import {
  faCopy,
  faEdit,
  faTrash,
  faBoxes,
  faIndent,
  faOutdent,
  faUsers,
  faMoneyBillAlt,
  faPallet,
  faUserEdit,
  faBars,
  faSyncAlt,
  faUpload,
  faWarehouse,
} from '@fortawesome/free-solid-svg-icons';

// Components import
import { Row, Container, Col } from 'reactstrap';

// Route components
import Finance from './components/Finance/Finance';
import Orders from './components/Orders/Orders';
import Stockinout from './components/Stockinout/Stockinout';
import SPInformation from './components/SPInformation/SPInformation';
import Users from './components/Users/Users';
import Roles from './components/Roles/Roles';
import Warehouse from './components/Warehouse/Warehouse';

// Partials components
import CustomNavbar from './components/Partials/Navbar';
import Sidebar from './components/Partials/Sidebar';
import './App.css';

library.add(
  faCopy,
  faEdit,
  faTrash,
  faBoxes,
  faIndent,
  faOutdent,
  faUsers,
  faMoneyBillAlt,
  faPallet,
  faUserEdit,
  faBars,
  faSyncAlt,
  faUpload,
  faWarehouse,
);

class App extends Component {
  render() {
    const { authorized } = this.props;
    if (!authorized) {
      return <Redirect to="/login" />;
    }
    return (
      <div>
        <CustomNavbar />
        <Container fluid>
          <Row>
            <Col xs="1" className="px-0 py-0">
              <Sidebar />
            </Col>
            <Col xs="10">
              <div className=" mt-3 px-5 py-3">
                <Switch>
                  <Route path="/orders" component={Orders} />
                  <Route path="/finance" component={Finance} />
                  <Route
                    path="/stockout"
                    component={() => {
                      return <Stockinout onChange={() => console.log()} />;
                    }}
                  />
                  <Route
                    path="/stockin"
                    component={() => {
                      return <Stockinout onChange={() => console.log()} />;
                    }}
                  />
                  <Route path="/users" component={Users} />
                  <Route path="/roles" component={Roles} />
                  <Route path="/spinformation" component={SPInformation} />
                  <Route path="/warehouse" component={Warehouse} />
                </Switch>
              </div>
            </Col>
            <Col xs="1" className="mt-1" />
          </Row>
        </Container>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return { authorized: state.token.authorized };
};

export default connect(mapStateToProps)(App);
