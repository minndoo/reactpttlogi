import { applyMiddleware, createStore, compose } from 'redux';
import { connectRouter, routerMiddleware } from 'connected-react-router';
import createHistory from 'history/createBrowserHistory';
import ReduxThunk from 'redux-thunk';
import Reducers from './Reducers/Reducers';
import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';

const history = createHistory();

const middleware = compose(
  applyMiddleware(routerMiddleware(history), ReduxThunk),
);

const persistConfig = {
  key: 'root',
  storage,
};
const persistedReducer = persistReducer(persistConfig, Reducers);

let store = createStore(
  connectRouter(history)(persistedReducer),
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
  middleware,
);
let persistor = persistStore(store);

export default { store, persistor };
