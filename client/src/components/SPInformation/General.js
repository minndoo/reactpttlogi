import React, { Component } from 'react';
import { Col, Row } from 'reactstrap';

import CollapseSearch from './CollapseSearch';
import CollapseTable from './CollapseTable';
import CollapseCreateTransaction from './CollapseCreateTransaction';

class General extends Component {
  render() {
    return (
      <div>
        <Row>
          <Col xs="12" className="px-0 py-0">
            <h2 className="float-left">Informace o paletách</h2>
          </Col>
        </Row>
        <Row>
          <Col xs="12" className="px-0">
            <CollapseSearch />
          </Col>
        </Row>
        <Row>
          <Col xs="12" className="px-0">
            <CollapseCreateTransaction />
          </Col>
        </Row>
        <Row>
          <Col xs="12" className="px-0">
            <CollapseTable />
          </Col>
        </Row>
      </div>
    );
  }
}

export default General;
