import React, { Component } from 'react';

//components import
import {
  Row,
  Input,
  Label,
  Col,
  Button,
  UncontrolledCollapse,
} from 'reactstrap';

import { connect } from 'react-redux';
import { toggleAuthorize } from '../../actions/loginActions';
import {
  handleInput,
  clearForm,
  setWarehouses,
} from '../../actions/SPIActions';
//actions import

class CollapseCreateTransaction extends Component {
  constructor(props) {
    super(props);
    this.state = {
      warehouses: [],
    };
  }
  componentDidMount() {
    const { token, toggleAuthorize, setWarehouses } = this.props;
    fetch('/warehouse', {
      method: 'GET',
      mode: 'cors',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        'Content-Length': '2000',
        Authorization: `Bearer ${token}`,
      },
    })
      .then(response => response.json())
      .then(data => {
        if (data.message !== 'Unauthorized') {
          setWarehouses(data.result[0].name);
          return this.setState({
            warehouses: data.result,
          });
        }
        return toggleAuthorize();
      });
  }
  render() {
    const { warehouses } = this.state;
    const { handleInput, clearForm, transaction } = this.props;
    return (
      <div>
        <div className="mt-2">
          <div
            id="CreateTransactionToggler"
            className="btn btn-secondary btn-block rounded-0"
          >
            Vytvořit transakci
          </div>
          <UncontrolledCollapse toggler="CreateTransactionToggler">
            <div className="bg-white px-3">
              <Row>
                <Col xs="6">
                  <Label>Počet přijatých palet</Label>
                  <Input
                    placeholder="Pocet palet"
                    name="TRANSACTION_PALLET_ACCEPT"
                    type="number"
                    onChange={e => {
                      handleInput(e);
                    }}
                    value={transaction.pallet_accept}
                  />
                </Col>
                <Col xs="6">
                  <Label>Sklad</Label>
                  <Input
                    type="select"
                    name="TRANSACTION_WAREHOUSE_ACCEPT"
                    onChange={e => {
                      handleInput(e);
                    }}
                    value={transaction.warehouse_accept}
                  >
                    {warehouses.length !== 0 &&
                      warehouses.map((warehouse, index) => {
                        return (
                          <option key={index} value={warehouse.name}>
                            {warehouse.name}
                          </option>
                        );
                      })}
                  </Input>
                </Col>
              </Row>
              <Row>
                <Col xs="6">
                  <Label>Počet odevzdaných palet</Label>
                  <Input
                    placeholder="Pocet palet"
                    name="TRANSACTION_PALLET_AWAY"
                    type="number"
                    onChange={e => {
                      handleInput(e);
                    }}
                    value={transaction.pallet_away}
                  />
                </Col>
                <Col xs="6">
                  <Label>Sklad</Label>
                  <Input
                    type="select"
                    name="TRANSACTION_WAREHOUSE_AWAY"
                    onChange={e => {
                      handleInput(e);
                    }}
                    value={transaction.warehouse_away}
                  >
                    {warehouses.length !== 0 &&
                      warehouses.map((warehouse, index) => {
                        return (
                          <option key={index} value={warehouse.name}>
                            {warehouse.name}
                          </option>
                        );
                      })}
                  </Input>
                </Col>
              </Row>
              <Row className="mt-2">
                <Col xs="8" />
                <Col xs="4" className="text-right my-2">
                  <Button
                    color="success"
                    className="mx-1"
                    onClick={() => {
                      clearForm();
                    }}
                  >
                    Vytvořit novou transakci
                  </Button>
                </Col>
              </Row>
            </div>
          </UncontrolledCollapse>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    token: state.token.token,
    transaction: state.SPinformationReducer,
  };
};

export default connect(
  mapStateToProps,
  { toggleAuthorize, handleInput, clearForm, setWarehouses },
)(CollapseCreateTransaction);
