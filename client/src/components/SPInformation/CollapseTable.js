import React, { Component } from 'react';
import { connect } from 'react-redux';

import { Table, Row, Col, UncontrolledCollapse } from 'reactstrap';

class CollapseTable extends Component {
  render() {
    return (
      <div className="mt-2">
        <div
          id="SPDataTableToggler"
          className="btn btn-secondary btn-block rounded-0"
        >
          Tabulka
        </div>
        <UncontrolledCollapse toggler="#SPDataTableToggler">
          <div className="bg-white">
            <Row>
              <Col xs="12">
                <Table bordered hover size="sm" className="text-center mb-0">
                  <thead>
                    <tr>
                      <th scope="col" rowSpan="2" className="align-middle">
                        Datum
                      </th>
                      <th scope="col" colSpan="3">
                        Palety
                      </th>
                      <th scope="col" colSpan="3">
                        Kartony
                      </th>
                      <th scope="col" rowSpan="2" className="align-middle">
                        Sklad
                      </th>
                    </tr>
                    <tr>
                      <th scope="col">Příjem</th>
                      <th scope="col">Výdej</th>
                      <th scope="col">Zůstatek</th>
                      <th scope="col">Příjem</th>
                      <th scope="col">Výdej</th>
                      <th scope="col">Zůstatek</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>24. 12. 2012</td>
                      <td>12 ks</td>
                      <td>10 ks</td>
                      <td>2 ks</td>
                      <td>12 ks</td>
                      <td>10 ks</td>
                      <td>2 ks</td>
                      <td>Sklad 1</td>
                    </tr>
                    <tr>
                      <td>24. 12. 2012</td>
                      <td>12 ks</td>
                      <td>10 ks</td>
                      <td>2 ks</td>
                      <td>480 Kč</td>
                      <td>400 Kč</td>
                      <td>48 Kč</td>
                      <td>Sklad 1</td>
                    </tr>
                  </tbody>
                </Table>
              </Col>
            </Row>
          </div>
        </UncontrolledCollapse>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    router: state.router,
  };
}

export default connect(mapStateToProps)(CollapseTable);
