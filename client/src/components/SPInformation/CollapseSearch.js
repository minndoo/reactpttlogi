import React, { Component } from "react";
import { connect } from "react-redux";

//Components import
import {
  Col,
  Row,
  UncontrolledCollapse,
  Input,
  Label,
  Button
} from "reactstrap";

//actions import
import { handleInput } from "../../actions/FinanceActions";

class CollapseSearch extends Component {
  render() {
    return (
      <div>
        <div
          id="SPSearchToggler"
          className="btn btn-secondary btn-block rounded-0"
        >
          Vyhledávač
        </div>
        <UncontrolledCollapse toggler="#SPSearchToggler">
          <div
            style={{ width: "100%" }}
            className="bg-white border rounded-0 px-3 py-3"
          >
            <Row>
              <Col xs="6">
                <Label>Dodavatel</Label>
                <Input name="CHANGE_SEARCH_SUPPLIER" />
              </Col>
              <Col xs="6">
                <Label>Datum</Label>
                <Input name="CHANGE_SEARCH_DATE" type="date" />
              </Col>
            </Row>
            <Row className="mt-3">
              <Col xs="12">
                <Button block color="primary">
                  Vyhledat
                </Button>
              </Col>
            </Row>
          </div>
        </UncontrolledCollapse>
      </div>
    );
  }
}

export default connect(
  null,
  { handleInput }
)(CollapseSearch);
