import React, { Component } from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

import General from './General';

class SPInformation extends Component {
  render() {
    const { authorized } = this.props;
    if (!authorized) {
      return <Redirect to="/login" />;
    }
    return (
      <div>
        <Switch>
          <Route exact path="/spinformation" component={General} />
        </Switch>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return { authorized: state.token.authorized };
};

export default connect(mapStateToProps)(SPInformation);
