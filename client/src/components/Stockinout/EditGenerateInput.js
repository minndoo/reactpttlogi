import React, { Component } from "react";
import { connect } from "react-redux";

//Components import
import { Label, Input, InputGroup } from "reactstrap";

// Actions import
import { handleInput } from "../../actions/StockinoutActions";

class EditGenerateInput extends Component {
  onCLickHandle = () => {};
  render() {
    return (
      <div>
        <Label>Cislo prijemky</Label>
        <InputGroup>
          <Input
            defaultValue={this.props.stockinout_number}
            onChange={this.props.handleInput}
            name="STOCKINOUT_NUMBER_CHANGE"
          />
        </InputGroup>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return { stockinout_number: state.StockinoutReducer.stockinout_number };
}

export default connect(
  mapStateToProps,
  { handleInput }
)(EditGenerateInput);
