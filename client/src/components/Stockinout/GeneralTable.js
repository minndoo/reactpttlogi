import React, { Component } from 'react';
import { connect } from 'react-redux';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { Table, Button, Row, UncontrolledCollapse } from 'reactstrap';

class GeneralTable extends Component {
  render() {
    const { data } = this.props;
    return (
      <div className="my-2">
        <div
          id="StockinoutTableToggler"
          className="btn btn-secondary btn-block rounded-0"
        >
          Tabulka
        </div>
        <UncontrolledCollapse toggler="#StockinoutTableToggler">
          <div className="px-3 bg-white">
            <Row>
              <Table size="sm" hover>
                <thead>
                  <tr>
                    <th>Číslo prijemky</th>
                    <th>Datum vytvoření</th>
                    <th>Poslední úprava</th>
                    <th />
                    <th />
                    <th />
                  </tr>
                </thead>
                <tbody>
                  {data.length !== 0 &&
                    data.map((stock, index) => {
                      return (
                        <tr key={index}>
                          <td>{stock._id}</td>
                          <td>{stock.delay_delivery_date}</td>
                          <td>{stock.delay_delivery_date}</td>
                          <td>
                            <Button color="link" className="py-0">
                              <FontAwesomeIcon
                                className="text-primary"
                                icon="copy"
                              />
                            </Button>
                          </td>
                          <td>
                            <Button color="link" className="py-0">
                              <FontAwesomeIcon
                                className="text-warning"
                                icon="edit"
                              />
                            </Button>
                          </td>
                          <td>
                            <Button color="link" className="py-0">
                              <FontAwesomeIcon
                                className="text-danger"
                                icon="trash"
                              />
                            </Button>
                          </td>
                        </tr>
                      );
                    })}
                </tbody>
              </Table>
            </Row>
          </div>
        </UncontrolledCollapse>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    router: state.router,
  };
}

export default connect(mapStateToProps)(GeneralTable);
