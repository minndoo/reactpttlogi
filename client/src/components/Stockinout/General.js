import React, { Component } from 'react';

import { Col, Row } from 'reactstrap';

import GeneralTable from './GeneralTable';
import GeneralSearch from './GeneralSearch';

class General extends Component {
  render() {
    const { match } = this.props;
    return (
      <div>
        <Row>
          <Col xs="6" className="px-0 py-0">
            <h2 className="float-left">
              {match.url === '/stockin' ? 'Stock In' : 'Stock out'}
            </h2>
          </Col>
        </Row>
        <Row className="mt-2">
          <Col xs="12" className="px-0">
            <GeneralSearch />
          </Col>
        </Row>
        <Row className="mt-2">
          <Col xs="12" className="px-0">
            <GeneralTable data={this.props.data} />
          </Col>
        </Row>
      </div>
    );
  }
}

export default General;
