import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
//Components import
import { Col, Row, Input, Label, Button } from 'reactstrap';
import EditGenerateInput from './EditGenerateInput';

//Actions import
import {
  handleInput,
  calculatePalletes,
  calculateBoxes,
  setStockinoutType,
} from '../../actions/StockinoutActions';
import { handleFetchDataOrderOne } from '../../actions/OrderActions';
import { setOrderId } from '../../actions/StockinoutActions';
import { toggleAuthorize } from '../../actions/loginActions';
class Edit extends Component {
  constructor(props) {
    super(props);
    this.postNewStock = this.postNewStock.bind(this);
  }
  postNewStock() {
    const { match, state, token, toggleAuthorize } = this.props;
    const id = state._id !== '' ? '/' + state._id : '';
    const splitUrl = '/' + match.url.split('/')[1];
    fetch(splitUrl + id, {
      method: 'POST',
      mode: 'cors',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        'Content-Length': '2000',
        Authorization: `Bearer ${token.token}`,
      },
      body: JSON.stringify(state),
    })
      .then(res => res.json())
      .then(data => {
        if (data.message === 'Unauthorized') {
          toggleAuthorize();
        }
      });
  }
  componentDidMount() {
    const {
      match,
      state,
      token,
      toggleAuthorize,
      handleFetchDataOrderOne,
    } = this.props;
    const orderId = match.params.orderId;
    let id = state.order_id;
    if (orderId) {
      id = orderId;
      this.props.setOrderId(orderId);
    }
    const stockinoutValue = match.url.split('/')[1];
    this.props.setStockinoutType(stockinoutValue);
    fetch(`/orders/${id}`, {
      method: 'GET',
      mode: 'cors',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        'Content-Length': '2000',
        Authorization: `Bearer ${token}`,
      },
    })
      .then(response => response.json())
      .then(data => {
        if (data.message !== 'Unauthorized') {
          return handleFetchDataOrderOne(data.result);
        } else {
          return toggleAuthorize();
        }
      });
  }
  render() {
    const { handleInput, order, state, match } = this.props;
    const rerouteUrl = match.url.split('/')[1];
    if (order === undefined) {
      return <div>No Data</div>;
    }
    return (
      <div>
        {' '}
        <form>
          <Row>
            <Col xs="4">
              <EditGenerateInput />
            </Col>
            <Col xs="4">
              <Label>ICO</Label>
              <Input readOnly value={order.identification_number} />
            </Col>
            <Col xs="4">
              <Label>Cislo objednavky</Label>
              <Input readOnly value={order.order_number} />
            </Col>
          </Row>
          <Row className="mt-3">
            <Col xs="12">
              <Label>Popis</Label>
              <Input type="textarea" readOnly value={order.description} />
            </Col>
          </Row>
          <Row className="mt-3">
            <Col>
              <Label>Zdrojovy doklad</Label>
              <Input readOnly value={order.source_document} />
            </Col>
            <Col>
              <Label>Datum vytvoreni objednavky</Label>
              <Input type="date" readOnly value={order.delivery_date} />
            </Col>
            <Col>
              <Label>Nahrane soubory</Label>
              <Input readOnly />
            </Col>
          </Row>
          <Row className="mt-3">
            <Col xs="6">
              <Label>Datum dodani</Label>
              <Input type="date" readOnly value={order.delivery_date} />
            </Col>
            <Col xs="6">
              <Label>Datum pozdejsiho dodani</Label>
              <Input name="DELAY_DELIVERY_DATE_CHANGE" type="date" />
            </Col>
          </Row>
          <Row>
            <Col xs="3">
              <Label>Pocet palet</Label>
              <Input readOnly value={order.palletes} />
            </Col>
            <Col xs="3">
              <Label>Skutecny pocet palet</Label>
              <Input
                name="REAL_PALLET_CHANGE"
                type="number"
                onChange={e => {
                  handleInput(e);
                  const val2 = parseInt(e.target.value, 10);
                  this.props.calculatePalletes(order.palletes, val2);
                }}
              />
            </Col>
            <Col xs="3">
              <Label>Pocet kartonu</Label>
              <Input readOnly value={order.boxes} />
            </Col>
            <Col xs="3">
              <Label>Skutecny pocet kartonu</Label>
              <Input
                name="REAL_BOX_CHANGE"
                type="number"
                onChange={e => {
                  handleInput(e);
                  const val2 = parseInt(e.target.value, 10);
                  this.props.calculateBoxes(order.boxes, val2);
                }}
              />
            </Col>
          </Row>
          <Row className="mt-3">
            <Col xs="3" className="textbind(this)-right">
              <p>Rozdil palet :</p>
            </Col>
            <Col xs="3">
              <Input readOnly value={state.palletesDiff} />
            </Col>
            <Col xs="3" className="text-right">
              <p>Rozdil kartonu :</p>
            </Col>
            <Col xs="3">
              <Input readOnly value={state.boxesDiff} />
            </Col>
          </Row>
          <Row className="mt-3">
            <Col xs="12">
              <Button
                color="success"
                block
                tag={Link}
                to={`/${rerouteUrl}`}
                onClick={() => {
                  this.postNewStock();
                }}
              >
                Submit
              </Button>
            </Col>
          </Row>
        </form>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    state: state.StockinoutReducer,
    order: state.OrderReducer,
    token: state.token.token,
  };
}

export default connect(
  mapStateToProps,
  {
    handleInput,
    handleFetchDataOrderOne,
    calculatePalletes,
    setOrderId,
    calculateBoxes,
    setStockinoutType,
    toggleAuthorize,
  },
)(Edit);
