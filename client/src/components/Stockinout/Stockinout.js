import React, { Component } from 'react';
import { Switch, Route, withRouter, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import General from './General';
// import View from "./View";
import Edit from './Edit';
import { toggleAuthorize } from '../../actions/loginActions';
class Stockinout extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
    };
  }
  componentDidMount() {
    const { match, token, toggleAuthorize } = this.props;
    fetch(match.path, {
      method: 'GET',
      mode: 'cors',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        'Content-Length': '2000',
        Authorization: `Bearer ${token}`,
      },
    })
      .then(response => response.json())
      .then(data => {
        if (data.message !== 'Unauthorized') {
          return this.setState({
            data: data.result,
          });
        } else {
          return toggleAuthorize();
        }
      });
  }
  render() {
    const { match, authorized } = this.props;
    if (!authorized) {
      return <Redirect to="/login" />;
    }
    return (
      <Switch>
        <Route
          exact
          path={match.url}
          component={() => (
            <General data={this.state.data} match={this.props.match} />
          )}
        />
        {/* <Route path={`${match.url}/view/:id`} component={View} /> */}
        <Route path={`${match.url}/create/:orderId`} component={Edit} />
        <Route path={`${match.url}/:id/edit`} component={Edit} />
      </Switch>
    );
  }
}

const mapStateToProps = state => {
  return { token: state.token.token, authorized: state.token.authorized };
};

export default withRouter(
  connect(
    mapStateToProps,
    { toggleAuthorize },
  )(Stockinout),
);
