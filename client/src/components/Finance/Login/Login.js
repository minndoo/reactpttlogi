import React, { Component } from 'react';
import {
  Container,
  Row,
  Col,
  Card,
  CardBody,
  CardFooter,
  Input,
  Label,
  Button,
} from 'reactstrap';
import { connect } from 'react-redux';
import {
  handleInput,
  setToken,
  toggleAuthorize,
  setPermissions,
} from '../../actions/loginActions';
import { Link, Redirect } from 'react-router-dom';

import pttLogo from './pttlogo.png';
import './loginStyles.css';

class Login extends Component {
  postLogin() {
    const { token, setToken, setPermissions, toggleAuthorize } = this.props;
    fetch(`/login`, {
      method: 'POST',
      mode: 'cors',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        'Content-Length': '2000',
      },
      body: JSON.stringify({
        userlogin: token.userlogin,
        userpassword: token.userpassword,
      }),
    })
      .then(response => response.json())
      .then(data => {
        console.log(data);
        if (data.message !== 'Unauthorized') {
          setToken(data.token);
          toggleAuthorize();
        }
        // setPermissions(data.permissions);
      });
  }
  render() {
    const { handleInput, token } = this.props;
    if (this.props.token.authorized) {
      return <Redirect to="/" />;
    }
    return (
      <Container fluid className="pt-5">
        <Row>
          <Col xs="4" />
          <Col xs="4">
            <img
              src={pttLogo}
              alt="logo"
              className="mx-auto d-block my-5 logo"
            />
          </Col>
          <Col xs="4" />
        </Row>
        <Row>
          <Col xs="4" />
          <Col xs="4">
            <Card className="text-center">
              <CardBody className="bg-light">
                <Row>
                  <Col xs="12" className="mb-3">
                    <Label>Username</Label>
                    <Input
                      className="text-center"
                      value={token.userlogin}
                      name="CHANGE_USER_LOGIN"
                      onChange={handleInput}
                    />
                  </Col>
                  <Col>
                    <Label>Password</Label>
                    <Input
                      type="password"
                      className="text-center"
                      name="CHANGE_USER_PASSWORD"
                      value={token.userpassword}
                      onChange={handleInput}
                    />
                  </Col>
                </Row>
              </CardBody>
              <CardFooter>
                <Button
                  block
                  color="primary"
                  onClick={this.postLogin.bind(this)}
                  to="/"
                  tag={Link}
                >
                  Login
                </Button>
              </CardFooter>
            </Card>
          </Col>
          <Col xs="4" />
        </Row>
      </Container>
    );
  }
}

const mapStateToProps = state => {
  return { token: state.token };
};

export default connect(
  mapStateToProps,
  { handleInput, setToken, toggleAuthorize },
)(Login);
