import React, { Component } from 'react';
import { connect } from 'react-redux';

//Componenets import
import { Row, Col, Button, UncontrolledCollapse } from 'reactstrap';
import CollapsePriceTableView from './CollapsePriceTableView';
import CollapsePriceTableEdit from './CollapsePriceTableEdit';

//Actions import
import { togglePrice } from '../../actions/FinanceActions';

class CollapsePriceTable extends Component {
  render() {
    const { togglePriceEdit, togglePrice } = this.props;
    return (
      <div className="mt-2">
        <div
          id="PriceTableToggler"
          className="btn btn-secondary btn-block rounded-0"
        >
          Ceník
        </div>
        <UncontrolledCollapse toggler="#PriceTableToggler">
          <div className="bg-white">
            {togglePriceEdit && <CollapsePriceTableEdit />}
            {!togglePriceEdit && (
              <div>
                <CollapsePriceTableView />
                <Row>
                  <Col xs="12" className="text-right">
                    <Button
                      onClick={togglePrice}
                      color="primary"
                      className="my-2 mx-1"
                    >
                      Upravit cenik
                    </Button>
                  </Col>
                </Row>
              </div>
            )}
          </div>
        </UncontrolledCollapse>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    router: state.router,
    togglePriceEdit: state.FinancePriceReducer.toggle_price,
  };
}

export default connect(
  mapStateToProps,
  { togglePrice },
)(CollapsePriceTable);
