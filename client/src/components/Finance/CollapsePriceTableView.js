import React, { Component } from 'react';
import { connect } from 'react-redux';

import { Table, Row, Col } from 'reactstrap';

import { handleFetchFinancePrice } from '../../actions/FinanceActions';
import { toggleAuthorize } from '../../actions/loginActions';
class CollapsePriceTableView extends Component {
  componentDidMount() {
    const { handleFetchFinancePrice, toggleAuthorize, token } = this.props;
    fetch('/financePrice', {
      method: 'GET',
      mode: 'cors',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        'Content-Length': '2000',
        Authorization: `Bearer ${token}`,
      },
    })
      .then(response => response.json())
      .then(data => {
        if (data.message !== 'Unauthorized') {
          return handleFetchFinancePrice(data.result);
        } else {
          return toggleAuthorize();
        }
      });
  }
  render() {
    const { daily_price, accept_price, away_price } = this.props.prices;
    return (
      <div>
        <Row>
          <Col xs="12">
            <Table size="sm" bordered className="text-center mb-0">
              <tbody>
                <tr>
                  <th>Cena za skladovani</th>
                  <td>{daily_price} kc/paleta/den</td>
                </tr>
                <tr>
                  <th>Cena za prijem</th>
                  <td>{accept_price} kc/paleta</td>
                </tr>
                <tr>
                  <th>Cena za vydej</th>
                  <td>{away_price} kc/paleta</td>
                </tr>
              </tbody>
            </Table>
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    prices: state.FinancePriceReducer,
    token: state.token.token,
  };
};

export default connect(
  mapStateToProps,
  { handleFetchFinancePrice, toggleAuthorize },
)(CollapsePriceTableView);
