import React, { Component } from 'react';

//Components import
import { connect } from 'react-redux';
import { Col, Row } from 'reactstrap';
import { Switch, Route, Redirect } from 'react-router-dom';

// Route components import
import General from './General.js';

class Finance extends Component {
  render() {
    const { match, authorized } = this.props;
    if (!authorized) {
      return <Redirect to="/login" />;
    }
    return (
      <Row>
        <Col xs="12">
          <Switch>
            <Route exact path={`${match.url}`} component={General} />
          </Switch>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = state => {
  return { authorized: state.token.authorized };
};

export default connect(mapStateToProps)(Finance);
