import React, { Component } from 'react';

//Components import
import { Col, Row } from 'reactstrap';
import CollapseSearch from './CollapseSearch';
import CollapsePriceTable from './CollapsePriceTable';
import CollapseDataTable from './CollapseDataTable';

class General extends Component {
  render() {
    return (
      <div>
        <Row>
          <Col xs="12" className="px-0 py-0">
            <h2 className="float-left">Finance</h2>
          </Col>
        </Row>
        <Row className="mt-2">
          <Col xs="12" className="px-0">
            <CollapseSearch />
          </Col>
          <Col xs="12" className="px-0">
            <CollapsePriceTable />
          </Col>
          <Col xs="12" className="px-0" Roles>
            <CollapseDataTable />
          </Col>
        </Row>
      </div>
    );
  }
}

export default General;
