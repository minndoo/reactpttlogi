import React, { Component } from 'react';
import { connect } from 'react-redux';
import { handleFetchDataFinance } from '../../actions/FinanceActions';
import { toggleAuthorize } from '../../actions/loginActions';
import { Table, Row, Col, UncontrolledCollapse } from 'reactstrap';

class CollapseDataTable extends Component {
  componentDidMount() {
    const { token, handleFetchDataFinance, toggleAuthorize } = this.props;
    fetch('/finance', {
      method: 'GET',
      mode: 'cors',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        'Content-Length': '2000',
        Authorization: `Bearer ${token}`,
      },
    })
      .then(response => response.json())
      .then(data => {
        if (data.message !== 'Unauthorized') {
          return handleFetchDataFinance(data.result);
        } else {
          return toggleAuthorize();
        }
      });
  }
  render() {
    const { financeTransactions } = this.props;
    return (
      <div className="mt-2">
        <div
          id="DataTableToggler"
          className="btn btn-secondary btn-block rounded-0"
        >
          Tabulka
        </div>
        <UncontrolledCollapse toggler="#DataTableToggler">
          <div className="bg-white">
            <Row>
              <Col xs="12">
                <Table bordered hover size="sm" className="text-center mb-0">
                  <thead>
                    <tr>
                      <th scope="col" rowSpan="2" className="align-middle">
                        Datum
                      </th>
                      <th scope="col" colSpan="3">
                        Palety
                      </th>
                      <th scope="col" colSpan="3">
                        Cena
                      </th>
                    </tr>
                    <tr>
                      <th scope="col">Příjem</th>
                      <th scope="col">Výdej</th>
                      <th scope="col">Zůstatek</th>
                      <th scope="col">Suma za příjem</th>
                      <th scope="col">Suma za výdej</th>
                      <th scope="col">Suma za skladování</th>
                    </tr>
                  </thead>
                  <tbody>
                    {financeTransactions &&
                      financeTransactions.map(transaction => {
                        return (
                          <tr>
                            <td>{transaction.date}</td>
                            <td>{transaction.pallet_accept} ks</td>
                            <td>{transaction.pallet_away} ks</td>
                            <td>{transaction.pallet_stored} ks</td>
                            <td>{transaction.accept_sum} Kč</td>
                            <td>{transaction.away_sum} Kč</td>
                            <td>{transaction.daily_sum} Kč</td>
                          </tr>
                        );
                      })}
                  </tbody>
                </Table>
              </Col>
            </Row>
          </div>
        </UncontrolledCollapse>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    router: state.router,
    financeTransactions: state.FinanceReducer.data,
    token: state.token.token,
  };
}

export default connect(
  mapStateToProps,
  { handleFetchDataFinance, toggleAuthorize },
)(CollapseDataTable);
