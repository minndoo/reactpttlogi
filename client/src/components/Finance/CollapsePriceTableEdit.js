import React, { Component } from 'react';
import { connect } from 'react-redux';

//components import
import { Table, Row, Input, Col, Button } from 'reactstrap';

//actions import
import { togglePrice, handleInput } from '../../actions/FinanceActions';
import { toggleAuthorize } from '../../actions/loginActions';
class CollapsePriceTableView extends Component {
  postNewPrice = () => {
    const { prices, token, toggleAuthorize } = this.props;
    fetch('/financePrice', {
      method: 'POST',
      mode: 'cors',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        'Content-Length': '2000',
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify(prices),
    })
      .then(res => res.json())
      .then(data => {
        if (data.message === 'Unauthorized') {
          toggleAuthorize();
        }
      });
  };
  render() {
    const { handleInput, prices } = this.props;
    return (
      <div>
        <Row>
          <Col>
            <Table bordered size="sm" className="text-center mb-0">
              <tbody>
                <tr>
                  <th>Cena za skladovani</th>
                  <td>
                    <Input
                      defaultValue={prices.daily_price}
                      type="number"
                      name="CHANGE_DAILY_PRICE"
                      onChange={handleInput}
                    />
                  </td>
                </tr>
                <tr>
                  <th>Cena za prijem</th>
                  <td>
                    <Input
                      defaultValue={prices.accept_price}
                      type="number"
                      name="CHANGE_ACCEPT_PRICE"
                      onChange={handleInput}
                    />
                  </td>
                </tr>
                <tr>
                  <th>Cena za vydej</th>
                  <td>
                    <Input
                      defaultValue={prices.away_price}
                      type="number"
                      name="CHANGE_AWAY_PRICE"
                      onChange={handleInput}
                    />
                  </td>
                </tr>
              </tbody>
            </Table>
          </Col>
        </Row>
        <Row className="mt-2">
          <Col xs="8" />
          <Col xs="4" className="text-right my-2">
            <Button
              onClick={this.props.togglePrice}
              color="warning"
              className="mx-1"
            >
              Zrusit
            </Button>
            <Button
              onClick={() => {
                this.postNewPrice();
                this.props.togglePrice();
              }}
              color="success"
              className="mx-1"
            >
              Potvrdit
            </Button>
          </Col>
        </Row>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    prices: state.FinancePriceReducer,
    token: state.token.token,
  };
}

export default connect(
  mapStateToProps,
  { togglePrice, handleInput, toggleAuthorize },
)(CollapsePriceTableView);
