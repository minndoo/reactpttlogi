import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Col, Row, Input, Label, Button } from 'reactstrap';
import { connect } from 'react-redux';
import {
  addUserRole,
  removeUserRole,
  handleInput,
  handleFetchUser,
} from '../../actions/UserActions';
import { toggleAuthorize } from '../../actions/loginActions';

class Edit extends Component {
  constructor(props) {
    super(props);
    this.state = {
      roles: [],
    };
  }

  postNewUser() {
    const { match, user, token } = this.props;
    let checkUser = '';
    if (match.params.userId) {
      checkUser = '/' + match.params.userId;
    }
    fetch(`/users${checkUser}`, {
      method: 'POST',
      mode: 'cors',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        'Content-Length': '2000',
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify(user),
    });
  }
  getUserInfo() {
    const { match, token, toggleAuthorize, handleFetchUser } = this.props;
    if (!match.params.userId) {
      return;
    }
    fetch(match.url, {
      method: 'GET',
      mode: 'cors',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        'Content-Length': '2000',
        Authorization: `Bearer ${token}`,
      },
    })
      .then(res => res.json())
      .then(data => {
        if (data.message === 'Unauthorized') {
          return toggleAuthorize();
        }
        handleFetchUser(data.result);
      });
  }

  handleAddRole(roleToAdd) {
    const { roles } = this.state;
    const index = roles.findIndex(role => role === roleToAdd);
    console.log(index);
    const newArray = roles
      .slice(0, index)
      .concat(roles.slice(index + 1, roles.length));
    this.setState({
      roles: newArray,
    });
    this.props.addUserRole(roleToAdd);
  }
  handleRemoveRole(roleToRemove) {
    const { roles } = this.state;
    const newArray = roles;
    console.log(roleToRemove);
    newArray.push(roleToRemove);
    this.setState({
      roles: newArray,
    });
    this.props.removeUserRole(roleToRemove);
  }
  componentDidMount() {
    const { userRoles } = this.props.user;
    const { token, toggleAuthorize } = this.props;
    fetch('/roles', {
      method: 'GET',
      mode: 'cors',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        'Content-Length': '2000',
        Authorization: `Bearer ${token}`,
      },
    })
      .then(response => response.json())
      .then(data => {
        if (data.message === 'Unauthorized') {
          return toggleAuthorize();
        }
        const filterRoles = data.result.filter(
          role => !userRoles.includes(role.rolename),
        );

        this.setState({ roles: filterRoles.map(role => role.rolename) });
      });
    this.getUserInfo();
  }
  render() {
    const { user, handleInput } = this.props;
    const { roles } = this.state;
    return (
      <div>
        <Row>
          <Col xs="6">
            <Label>Jmeno</Label>
            <Input name="CHANGE_USER_NAME" onChange={handleInput} />
          </Col>
          <Col xs="6">
            <Label>Prijmeni</Label>
            <Input name="CHANGE_USER_SURNAME" onChange={handleInput} />
          </Col>
        </Row>
        <Row>
          <Col xs="5">
            <Label>Login</Label>
            <Input name="CHANGE_USER_LOGIN" onChange={handleInput} />
          </Col>
        </Row>
        <Row>
          <Col xs="6">
            <Label>Heslo</Label>
            <Input
              type="password"
              name="CHANGE_USER_PASSWORD"
              onChange={handleInput}
            />
          </Col>
          <Col xs="6">
            <Label>Potvrdit heslo</Label>
            <Input
              type="password"
              name="CHANGE_USER_PASSWORD_CONFIRM"
              onChange={handleInput}
            />
          </Col>
        </Row>
        <Row className="mt-3">
          <Col xs="6">
            <Label>Role</Label>
            <div className="bg-white border rounded px-3 py-3">
              {roles.length !== 0 &&
                roles.map((role, index) => {
                  return (
                    <Button
                      key={index}
                      color="primary"
                      block
                      onClick={() => this.handleAddRole(role)}
                    >
                      {role}
                    </Button>
                  );
                })}
            </div>
          </Col>
          <Col xs="6">
            <Label>Prirazene role</Label>
            <div className="bg-white border rounded px-3 py-3">
              {user.userRoles.map((role, key) => {
                return (
                  <Button
                    key={key}
                    color="primary"
                    block
                    onClick={() => this.handleRemoveRole(role)}
                  >
                    {role}
                  </Button>
                );
              })}
            </div>
          </Col>
        </Row>
        <Row className="mt-3">
          <Col xs="4" />
          <Col xs="4">
            <Button
              block
              color="success"
              onClick={this.postNewUser.bind(this)}
              to="/users"
              tag={Link}
            >
              Vytvorit
            </Button>
          </Col>
          <Col xs="4" />
        </Row>
      </div>
    );
  }
}

const mapStateToPros = state => {
  return {
    user: state.UserReducer,
    token: state.token.token,
  };
};

export default connect(
  mapStateToPros,
  {
    addUserRole,
    removeUserRole,
    handleInput,
    handleFetchUser,
    toggleAuthorize,
  },
)(Edit);
