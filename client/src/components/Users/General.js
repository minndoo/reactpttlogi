import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { Col, Row, Table, Button } from 'reactstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { clearUserForm } from '../../actions/UserActions';
import { toggleAuthorize } from '../../actions/loginActions';

class General extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
    };
    this.deleteUser = this.deleteUser.bind(this);
    this.updateUsers = this.updateUsers.bind(this);
  }
  deleteUser(user_id) {
    const { token, toggleAuthorize } = this.props;
    fetch(`/users/${user_id}`, {
      method: 'DELETE',
      mode: 'cors',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        'Content-Length': '2000',
        Authorization: `Bearer ${token}`,
      },
    })
      .then(res => res.json())
      .then(data => {
        if (data.message === 'Unauthorized') {
          toggleAuthorize();
        }
      });
  }
  updateUsers() {
    const { token, toggleAuthorize } = this.props;
    fetch('/users', {
      method: 'GET',
      mode: 'cors',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        'Content-Length': '2000',
        Authorization: `Bearer ${token}`,
      },
    })
      .then(response => response.json())
      .then(data => {
        if (data.message !== 'Unauthorized') {
          return this.setState({ data: data.result });
        } else {
          return toggleAuthorize();
        }
      });
  }

  componentDidMount() {
    this.updateUsers();
  }

  render() {
    const { data } = this.state;
    return (
      <div>
        <Row>
          <Col xs="6" className="text-left">
            <h2>Uživatele</h2>
          </Col>
          <Col xs="6" className="text-right">
            <Button
              color="success"
              to="/users/create"
              tag={Link}
              onClick={this.props.clearUserForm}
            >
              Vytvořit uživatele
            </Button>
          </Col>
        </Row>
        <Row className="mt-3">
          <Col xs="12">
            <Table size="sm" className="bg-white text-center">
              <thead>
                <tr>
                  <th>UID</th>
                  <th>JMENO</th>
                  <th>LOGIN</th>
                  <th>ROLE</th>
                  <th colSpan="2" />
                </tr>
              </thead>
              <tbody>
                {data.length !== 0 &&
                  data.map((user, index) => {
                    return (
                      <tr key={index}>
                        <td>{index}</td>
                        <td>{user.username + ' ' + user.usersurname}</td>
                        <td>{user.userlogin}</td>
                        <td>
                          {user.roles &&
                            user.roles.map((role, index) => {
                              return <span key={index}>{role} </span>;
                            })}
                        </td>
                        <td>
                          <Button color="link" className="py-0">
                            <FontAwesomeIcon
                              icon="edit"
                              className="text-warning"
                            />
                          </Button>
                        </td>
                        <td>
                          <Button
                            color="link"
                            className="py-0"
                            onClick={() => {
                              this.deleteUser(user._id);
                              this.updateUsers();
                            }}
                            to="/users"
                            tag={Link}
                          >
                            <FontAwesomeIcon
                              icon="trash"
                              className="text-danger"
                            />
                          </Button>
                        </td>
                      </tr>
                    );
                  })}
              </tbody>
            </Table>
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return { token: state.token.token };
};

export default connect(
  mapStateToProps,
  { clearUserForm, toggleAuthorize },
)(General);
