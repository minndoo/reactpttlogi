import React, { Component } from 'react';
import { Label, Input } from 'reactstrap';

import { connect } from 'react-redux';
import { handleCheck } from '../../actions/RoleAction';

class RolePermissions extends Component {
  render() {
    const { handleCheck, permissions } = this.props;
    return (
      <tr>
        <td>Role</td>
        <td>
          <Label>
            <Input
              className="mx-auto mt-0"
              type="checkbox"
              onChange={handleCheck}
              checked={permissions.view}
              name="ROLE_CHECK_VIEW"
            />
          </Label>
        </td>
        <td>
          <Label>
            <Input
              className="mx-auto mt-0"
              type="checkbox"
              onChange={handleCheck}
              checked={permissions.write}
              name="ROLE_CHECK_WRITE"
            />
          </Label>
        </td>
        <td>
          <Label>
            <Input
              className="mx-auto mt-0"
              type="checkbox"
              onChange={handleCheck}
              checked={permissions.edit}
              name="ROLE_CHECK_EDIT"
            />
          </Label>
        </td>
        <td>
          <Label>
            <Input
              className="mx-auto mt-0"
              type="checkbox"
              onChange={handleCheck}
              checked={permissions.delete}
              name="ROLE_CHECK_DELETE"
            />
          </Label>
        </td>
        <td>
          <Label>
            <Input
              className="mx-auto mt-0"
              type="checkbox"
              onChange={handleCheck}
              checked={permissions.manage}
              name="ROLE_CHECK_MANAGE"
            />
          </Label>
        </td>
      </tr>
    );
  }
}

const mapStateToProps = state => {
  return { permissions: state.RolesReducer.permissions.roles };
};

export default connect(
  mapStateToProps,
  { handleCheck },
)(RolePermissions);
