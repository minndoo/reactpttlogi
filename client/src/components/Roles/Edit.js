import React, { Component } from 'react';
import { Col, Row, Table, Label, Input, Button } from 'reactstrap';
import { Link } from 'react-router-dom';

import { connect } from 'react-redux';
import { handleInput, fetchExistingRole } from '../../actions/RoleAction';

import RolesPermissions from './RolePermissions';
import UserPermissions from './UserPermissions';
import FinancePermissions from './FinancePermissions';
import StockinPermissions from './StockinPermissions';
import StockoutPermissions from './StockoutPermissions';
import SPIPermissions from './SPIPermissions';
import OrderPermissions from './OrderPermissions';

import { toggleAuthorize } from '../../actions/loginActions';

class Edit extends Component {
  postNewRole() {
    const { match, role, token, toggleAuthorize } = this.props;
    let checkRole = '';
    if (match.params.role) {
      checkRole = '/' + match.params.role;
    }
    fetch(`/roles${checkRole}`, {
      method: 'POST',
      mode: 'cors',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        'Content-Length': '2000',
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify(role),
    })
      .then(res => res.json())
      .then(data => {
        if (data.message === 'Unauthorized') {
          return toggleAuthorize();
        }
      });
  }
  componentDidMount() {
    const { match, token, toggleAuthorize, fetchExistingRole } = this.props;
    if (match.params.role) {
      fetch(match.url, {
        method: 'GET',
        mode: 'cors',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          'Content-Length': '2000',
          Authorization: `Bearer ${token.token}`,
        },
      })
        .then(response => response.json())
        .then(data => {
          if (data.message !== 'Unauthorized') {
            return fetchExistingRole(data.result);
          } else {
            return toggleAuthorize();
          }
        });
    }
  }
  fetchExistingRole(data) {}
  render() {
    const { role, handleInput } = this.props;
    console.log(role);
    return (
      <div>
        <Row>
          <Label>Nazev role</Label>
          <Input
            placeholder="Nazev role"
            name="CHANGE_ROLE_NAME"
            onChange={handleInput}
            value={role.rolename}
          />
        </Row>
        <Row className="mt-3">
          <Col xs="12" className="bg-white">
            <Table className="text-center">
              <thead>
                <tr>
                  <th>Stránka</th>
                  <th>Číst</th>
                  <th>Přidávat</th>
                  <th>Opravit</th>
                  <th>Smazat</th>
                  <th>Správce</th>
                  <th />
                </tr>
              </thead>
              <tbody>
                <OrderPermissions />
                <FinancePermissions />
                <UserPermissions />
                <RolesPermissions />
                <StockinPermissions />
                <StockoutPermissions />
                <SPIPermissions />
              </tbody>
            </Table>
          </Col>
        </Row>
        <Row>
          <Col xs="12">
            <Button
              block
              color="success"
              onClick={this.postNewRole.bind(this)}
              to="/roles"
              tag={Link}
            >
              Vytvořit roli
            </Button>
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    role: state.RolesReducer,
    token: state.token.token,
  };
};

export default connect(
  mapStateToProps,
  { handleInput, fetchExistingRole, toggleAuthorize },
)(Edit);
