import React, { Component } from 'react';
import { Col, Row, Table, Button } from 'reactstrap';
import { Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { connect } from 'react-redux';
import { clearFormRole } from '../../actions/RoleAction';
import { toggleAuthorize } from '../../actions/loginActions';

class General extends Component {
  constructor(props) {
    super(props);
    this.state = {
      roles: [],
    };
    this.deleteRole = this.deleteRole.bind(this);
    this.updateRoles = this.updateRoles.bind(this);
  }
  deleteRole(role_id) {
    const { token, toggleAuthorize } = this.props;
    fetch(`/roles/${role_id}`, {
      method: 'DELETE',
      mode: 'cors',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        'Content-Length': '2000',
        Authorization: `Bearer ${token}`,
      },
    })
      .then(res => res.json())
      .then(data => {
        if (data.message === 'Unauthorized') {
          return toggleAuthorize();
        }
      });
  }
  updateRoles() {
    const { token, toggleAuthorize } = this.props;
    fetch('/roles', {
      method: 'GET',
      mode: 'cors',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        'Content-Length': '2000',
        Authorization: `Bearer ${token}`,
      },
    })
      .then(response => response.json())
      .then(data => {
        if (data.message !== 'Unauthorized') {
          return this.setState({
            roles: data.result,
          });
        } else {
          return toggleAuthorize();
        }
      });
  }
  componentDidMount() {
    this.updateRoles();
  }
  render() {
    const { roles } = this.state;
    return (
      <div>
        <Row>
          <Col xs="6" className="text-left">
            <h2>Role</h2>
          </Col>
          <Col xs="6" className="text-right">
            <Button
              color="success"
              to="/roles/create"
              tag={Link}
              onClick={this.props.clearFormRole}
            >
              Vytvorit novou roli
            </Button>
          </Col>
        </Row>
        <Row>
          <Col xs="12">
            <Table size="sm" className="bg-white text-center">
              <thead>
                <tr>
                  <th>Nazev role</th>
                  <th colSpan="2" />
                </tr>
              </thead>
              <tbody>
                {roles.length !== 0 &&
                  roles.map((role, index) => {
                    return (
                      <tr key={index}>
                        <td className="text-uppercase">{role.rolename}</td>
                        <td>
                          <Button
                            color="link"
                            to={`/roles/${role._id}`}
                            tag={Link}
                          >
                            <FontAwesomeIcon
                              className="text-warning"
                              icon="edit"
                            />
                          </Button>
                        </td>
                        <td>
                          <Button
                            color="link"
                            onClick={() => {
                              this.deleteRole(role._id);
                              this.updateRoles();
                            }}
                          >
                            <FontAwesomeIcon
                              className="text-danger"
                              icon="trash"
                            />
                          </Button>
                        </td>
                      </tr>
                    );
                  })}
              </tbody>
            </Table>
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return { token: state.token.token };
};

export default connect(
  mapStateToProps,
  { clearFormRole, toggleAuthorize },
)(General);
