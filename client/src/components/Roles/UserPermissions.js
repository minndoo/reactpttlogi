import React, { Component } from 'react';
import { Label, Input } from 'reactstrap';

import { connect } from 'react-redux';
import { handleCheck } from '../../actions/RoleAction';

class UserPermissions extends Component {
  render() {
    const { handleCheck, permissions } = this.props;
    return (
      <tr>
        <td>Uživatelé</td>
        <td>
          <Label>
            <Input
              className="mx-auto mt-0"
              type="checkbox"
              onChange={handleCheck}
              checked={permissions.view}
              name="USER_CHECK_VIEW"
            />
          </Label>
        </td>
        <td>
          <Label>
            <Input
              className="mx-auto mt-0"
              type="checkbox"
              onChange={handleCheck}
              checked={permissions.write}
              name="USER_CHECK_WRITE"
            />
          </Label>
        </td>
        <td>
          <Label>
            <Input
              className="mx-auto mt-0"
              type="checkbox"
              onChange={handleCheck}
              checked={permissions.edit}
              name="USER_CHECK_EDIT"
            />
          </Label>
        </td>
        <td>
          <Label>
            <Input
              className="mx-auto mt-0"
              type="checkbox"
              onChange={handleCheck}
              checked={permissions.delete}
              name="USER_CHECK_DELETE"
            />
          </Label>
        </td>
        <td>
          <Label>
            <Input
              className="mx-auto mt-0"
              type="checkbox"
              onChange={handleCheck}
              checked={permissions.manage}
              name="USER_CHECK_MANAGE"
            />
          </Label>
        </td>
      </tr>
    );
  }
}

const mapStateToProps = state => {
  return { permissions: state.RolesReducer.permissions.user };
};

export default connect(
  mapStateToProps,
  { handleCheck },
)(UserPermissions);
