import React, { Component } from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import General from './General';
import Edit from './Edit';

class Roles extends Component {
  render() {
    const { authorized } = this.props;
    if (!authorized) {
      return <Redirect to="/login" />;
    }
    return (
      <div>
        <Switch>
          <Route exact path="/roles" component={General} />
          <Route path="/roles/create" component={Edit} />
          <Route path="/roles/:role" component={Edit} />
        </Switch>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return { authorized: state.token.authorized };
};

export default connect(mapStateToProps)(Roles);
