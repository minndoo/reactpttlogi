import React, { Component } from 'react';
import { Label, Input } from 'reactstrap';

import { connect } from 'react-redux';
import { handleCheck } from '../../actions/RoleAction';

class FinancePermissions extends Component {
  render() {
    const { handleCheck, permissions } = this.props;
    return (
      <tr>
        <td>Finance</td>
        <td>
          <Label>
            <Input
              className="mx-auto mt-0"
              type="checkbox"
              onChange={handleCheck}
              checked={permissions.view}
              name="FINANCE_CHECK_VIEW"
            />
          </Label>
        </td>
        <td>
          <Label>
            <Input
              className="mx-auto mt-0"
              type="checkbox"
              onChange={handleCheck}
              checked={permissions.write}
              name="FINANCE_CHECK_WRITE"
            />
          </Label>
        </td>
        <td>
          <Label>
            <Input
              className="mx-auto mt-0"
              type="checkbox"
              onChange={handleCheck}
              checked={permissions.edit}
              name="FINANCE_CHECK_EDIT"
            />
          </Label>
        </td>
        <td>
          <Label>
            <Input
              className="mx-auto mt-0"
              type="checkbox"
              onChange={handleCheck}
              checked={permissions.delete}
              name="FINANCE_CHECK_DELETE"
            />
          </Label>
        </td>
        <td>
          <Label>
            <Input
              className="mx-auto mt-0"
              type="checkbox"
              onChange={handleCheck}
              checked={permissions.manage}
              name="FINANCE_CHECK_MANAGE"
            />
          </Label>
        </td>
      </tr>
    );
  }
}

const mapStateToProps = state => {
  return { permissions: state.RolesReducer.permissions.finance };
};

export default connect(
  mapStateToProps,
  { handleCheck },
)(FinancePermissions);
