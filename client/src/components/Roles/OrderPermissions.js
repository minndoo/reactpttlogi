import React, { Component } from 'react';
import { Label, Input } from 'reactstrap';

import { connect } from 'react-redux';
import { handleCheck } from '../../actions/RoleAction';

class OrderPermissions extends Component {
  render() {
    const { handleCheck, permissions } = this.props;
    return (
      <tr>
        <td>Objednávky</td>
        <td>
          <Label>
            <Input
              className="mx-auto mt-0"
              type="checkbox"
              checked={permissions.view}
              onChange={handleCheck}
              name="ORDER_CHECK_VIEW"
            />
          </Label>
        </td>
        <td>
          <Label>
            <Input
              className="mx-auto mt-0"
              type="checkbox"
              onChange={handleCheck}
              checked={permissions.write}
              name="ORDER_CHECK_WRITE"
            />
          </Label>
        </td>
        <td>
          <Label>
            <Input
              className="mx-auto mt-0"
              type="checkbox"
              onChange={handleCheck}
              checked={permissions.edit}
              name="ORDER_CHECK_EDIT"
            />
          </Label>
        </td>
        <td>
          <Label>
            <Input
              className="mx-auto mt-0"
              type="checkbox"
              onChange={handleCheck}
              checked={permissions.delete}
              name="ORDER_CHECK_DELETE"
            />
          </Label>
        </td>
        <td>
          <Label>
            <Input
              className="mx-auto mt-0"
              type="checkbox"
              onChange={handleCheck}
              checked={permissions.manage}
              name="ORDER_CHECK_MANAGE"
            />
          </Label>
        </td>
      </tr>
    );
  }
}

const mapStateToProps = state => {
  return { permissions: state.RolesReducer.permissions.order };
};

export default connect(
  mapStateToProps,
  { handleCheck },
)(OrderPermissions);
