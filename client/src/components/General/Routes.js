import React, { Component } from 'react';
import { connect } from 'react-redux';
// Components Import
import { NavItem, NavLink, Row, Col } from 'reactstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Link } from 'react-router-dom';

// Icons import

//Actions import

const routes = [
  {
    path: '/orders',
    name: 'Objednávky',
    icon: 'boxes',
  },
  {
    path: '/finance',
    name: 'Finance',
    icon: 'money-bill-alt',
  },
  {
    path: '/stockout',
    name: 'Výdejky',
    icon: 'outdent',
  },
  {
    path: '/stockin',
    name: 'Příjemky',
    icon: 'indent',
  },
  {
    path: '/spinformation',
    name: 'Informace o paletách',
    icon: 'pallet',
  },
  {
    path: '/users',
    name: 'Uživatelé',
    icon: 'users',
  },
  {
    path: '/roles',
    name: 'Role',
    icon: 'user-edit',
  },
  {
    path: '/warehouse',
    name: 'Sklady',
    icon: 'warehouse',
  },
];

class Routes extends Component {
  render() {
    return (
      <div>
        {routes.map((route, index) => {
          return (
            <NavItem key={index}>
              <NavLink to={route.path} tag={Link}>
                <Row>
                  <Col xs="1">
                    <FontAwesomeIcon icon={route.icon} />
                  </Col>
                  <Col xs="9">
                    <small>{route.name}</small>
                  </Col>
                </Row>
              </NavLink>
            </NavItem>
          );
        })}
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    current_url: state.router,
  };
}

export default connect(mapStateToProps)(Routes);
