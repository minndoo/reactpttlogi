import React, { Component } from "react";
import MotionMenu from "react-motion-menu";

class MotionMenuJS extends Component {
  render() {
    return (
      <MotionMenu type="circle" margin={120}>
        <div className="button">
          <i className="fa fa-bars" />
        </div>
        <div className="button">
          <i className="fa fa-cogs" />
        </div>
        <div className="button">
          <i className="fa fa-cloud" />
        </div>
        <div className="button">
          <i className="fa fa-home" />
        </div>
      </MotionMenu>
    );
  }
}

export default MotionMenuJS;
