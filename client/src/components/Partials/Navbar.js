import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

//Components import
import { Navbar, NavbarToggler, Nav, NavItem, NavbarBrand } from 'reactstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
//Actions import
import { toggleSidebar } from '../../actions/DOMActions';

class CustomNavbar extends Component {
  render() {
    return (
      <div className="border-bottom">
        <Navbar>
          <Nav>
            <NavItem>
              <NavbarToggler onClick={this.props.toggleSidebar}>
                <div className="btn btn-outline-primary py-1">
                  <FontAwesomeIcon icon="bars" />
                </div>
              </NavbarToggler>
            </NavItem>
            <NavItem>
              <NavbarBrand to="/" tag={Link}>
                PTTLogi
              </NavbarBrand>
            </NavItem>
          </Nav>
        </Navbar>
      </div>
    );
  }
}

export default connect(
  null,
  { toggleSidebar },
)(CustomNavbar);
