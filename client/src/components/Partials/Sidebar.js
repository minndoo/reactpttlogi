import React from "react";
import { connect } from "react-redux";

//Components import
import { Nav } from "reactstrap";
import Routes from "../General/Routes";
import "./Sidebar.css";

class Sidebar extends React.Component {
  render() {
    return (
      <div className="bg-light fullheight" hidden={this.props.sidebar_open}>
        <Nav vertical>
          <Routes />
        </Nav>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return { sidebar_open: state.DOMReducer.sidebar_open };
}

export default connect(mapStateToProps)(Sidebar);
