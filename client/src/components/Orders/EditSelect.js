import React, { Component } from 'react';
import { connect } from 'react-redux';

// Components import
import { Label, Input } from 'reactstrap';

//import Action
import { handleInput } from '../../actions/OrderActions';

class EditSelect extends Component {
  render() {
    return (
      <div>
        <Label>Typ objednavky</Label>
        <Input
          type="select"
          onChange={this.props.handleInput}
          name="ORDER_TYPE_CHANGE"
          value={`${this.props.order_type}`}
        >
          <option value="received">Přijatá</option>
          <option value="given">Vydaná</option>
        </Input>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return { order_type: state.OrderReducer.order_type };
}

export default connect(
  mapStateToProps,
  { handleInput },
)(EditSelect);
