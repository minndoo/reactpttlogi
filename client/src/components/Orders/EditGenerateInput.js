import React, { Component } from 'react';
import { connect } from 'react-redux';
import uniqid from 'uniqid';

//Components import
import { Label, Input, InputGroup, InputGroupAddon, Button } from 'reactstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

// Actions import
import { handleInput, handleInputGenerated } from '../../actions/OrderActions';

class EditGenerateInput extends Component {
  onCLickHandle = e => {
    const generated = uniqid('ORD-');
    this.props.handleInputGenerated(generated);
  };
  render() {
    return (
      <div>
        <Label>Cislo objednavky</Label>
        <InputGroup>
          <Input
            placeholder="ordernumber"
            value={this.props.order_number}
            onChange={this.props.handleInput}
            name="ORDER_NUMBER_CHANGE"
            required
          />
          <InputGroupAddon addonType="append">
            <Button onClick={this.onCLickHandle.bind(this)}>
              <FontAwesomeIcon icon="sync-alt" />
            </Button>
          </InputGroupAddon>
        </InputGroup>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return { order_number: state.OrderReducer.order_number };
}

export default connect(
  mapStateToProps,
  { handleInput, handleInputGenerated },
)(EditGenerateInput);
