import React, { Component } from 'react';
import { connect } from 'react-redux';

//Components import
import { Col, Row, Input, Label, Button } from 'reactstrap';
import EditGenerateInput from './EditGenerateInput';
import EditSelect from './EditSelect';
import FileUpload from './FileUpload';
import { Link } from 'react-router-dom';

//Actions import
import {
  handleInput,
  handleFetchDataOrderOne,
} from '../../actions/OrderActions';
import { toggleAuthorize } from '../../actions/loginActions';
// styles

class Edit extends Component {
  componentDidMount() {
    const {
      match,
      handleFetchDataOrderOne,
      toggleAuthorize,
      token,
    } = this.props;
    if (match.params.orderId) {
      fetch(`/orders/${match.params.orderId}`, {
        method: 'GET',
        mode: 'cors',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          'Content-Length': '2000',
          Authorization: `Bearer ${token}`,
        },
      })
        .then(response => response.json())
        .then(data => {
          if (data.message !== 'Unauthorized') {
            return handleFetchDataOrderOne(data.result);
          } else {
            return toggleAuthorize();
          }
        });
    }
  }
  postOrder() {
    const { order, token, toggleAuthorize } = this.props;
    const { orderId } = this.props.match.params;
    let checkOrder = '';
    if (orderId) {
      checkOrder = '/' + orderId;
    }
    fetch(`/orders${checkOrder}`, {
      method: 'POST',
      mode: 'cors',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        'Content-Length': '2000',
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify(order),
    })
      .then(res => res.json())
      .then(data => {
        if (data.message === 'Unauthorized') {
          toggleAuthorize();
        }
      });
  }
  render() {
    const { handleInput, order } = this.props;
    return (
      <div>
        <form>
          {/* 1st row */}
          <Row>
            <Col xs="5">
              <EditGenerateInput />
            </Col>
            <Col xs="2">
              <EditSelect />
            </Col>
            <Col xs="5">
              <Label>ICO</Label>
              <Input
                onChange={handleInput}
                name="IDENTIFICATION_NUMBER_CHANGE"
                value={order.identification_number}
                required
              />
            </Col>
          </Row>
          {/* 2nd row */}
          <Row className="mt-3">
            <Col xs="6">
              <Label>Adresat</Label>
              <Input
                placeholder="Adresat"
                onChange={handleInput}
                name="ADDRESSEE_CHANGE"
                value={order.addressee}
                required
              />
            </Col>
            <Col xs="6">
              <Label>
                {order.order_type === 'received' ? 'Dodavatel' : 'Odberatel'}
              </Label>
              <Input
                placeholder={
                  order.order_type === 'received' ? 'Dodavatel' : 'Odberatel'
                }
                onChange={handleInput}
                name={
                  order.order_type === 'received' ? 'SUP_CHANGE' : 'SUB_CHANGE'
                }
                value={
                  order.order_type === 'received'
                    ? order.supplier
                    : order.subscriber
                }
              />
            </Col>
          </Row>
          {/* 3rd row */}
          <Row className="mt-3">
            <Col xs="12">
              <Label>Popis</Label>
              <Input
                type="textarea"
                onChange={handleInput}
                name="DESCRIPTION_CHANGE"
                value={order.description}
              />
            </Col>
          </Row>
          {/* 4th row */}
          <Row className="mt-3">
            <Col>
              <Label>Zdrojovy doklad</Label>
              <Input
                name="SOURCE_DOCUMENT_CHANGE"
                onChange={handleInput}
                value={order.source_document}
              />
            </Col>
            <Col>
              <Label>Datum dodani</Label>
              <Input
                type="date"
                name="DATE_CHANGE"
                onChange={handleInput}
                value={order.delivery_date}
              />
            </Col>
            <Col>
              <Label>Soubory k nahrani</Label>
              <FileUpload />
            </Col>
          </Row>
          {/* 5th row */}
          <Row className="mt-3">
            <Col xs="3">
              <Label>Pocet palet</Label>
              <Input
                name="PALLET_CHANGE"
                type="number"
                onChange={handleInput}
                value={order.palletes}
              />
            </Col>
            <Col xs="3" />
            <Col xs="6">
              <Label>Urceno pro</Label>
              <Input
                name="MEANT_FOR_CHANGE"
                onChange={handleInput}
                value={order.meant_for}
              />
            </Col>
          </Row>
          {/* 6th row */}
          <Row className="mt-3">
            <Col xs="3">
              <Label>Pocet kartonu</Label>
              <Input
                name="BOX_CHANGE"
                type="number"
                onChange={handleInput}
                value={order.boxes}
              />
            </Col>
          </Row>
          {/* submit button */}
          <Row className="mt-3">
            <Col xs="12">
              <Button
                color="success"
                onClick={() => {
                  this.postOrder();
                }}
                to="/orders"
                tag={Link}
                block
              >
                Vytvořit objednávku
              </Button>
            </Col>
          </Row>
        </form>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    order: state.OrderReducer,
    router: state.router,
    token: state.token.token,
  };
}

export default connect(
  mapStateToProps,
  {
    handleInput,
    handleFetchDataOrderOne,
    toggleAuthorize,
  },
)(Edit);
