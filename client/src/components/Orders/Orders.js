import React, { Component } from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import General from './General';
import View from './View';
import Edit from './Edit';
import { connect } from 'react-redux';
class Orders extends Component {
  render() {
    const { match, authorized } = this.props;
    if (!authorized) {
      return <Redirect to="/login" />;
    }
    return (
      <Switch>
        <Route exact path={match.url} component={General} />
        <Route exact path={`${match.url}/create`} component={Edit} />
        <Route exact path={`${match.url}/:orderId`} component={View} />
        <Route exact path={`${match.url}/:orderId/edit`} component={Edit} />
      </Switch>
    );
  }
}

const mapStateToProps = state => {
  return {
    authorized: state.token.authorized,
  };
};

export default connect(mapStateToProps)(Orders);
