import React, { Component } from 'react';
class View extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {},
    };
  }
  componentDidMount() {
    const { orderId } = this.props.match.params;
    fetch(`/orders/${orderId}`).then(response => console.log(response));
    //   .then(data =>
    //     this.setState({
    //       data: data.result,
    //     }),
    //   );
  }
  render() {
    return <div>View</div>;
  }
}

export default View;
