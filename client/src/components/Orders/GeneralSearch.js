import React, { Component } from "react";

import {
  Button,
  UncontrolledCollapse,
  Row,
  Col,
  Label,
  Input
} from "reactstrap";

class GeneralSearch extends Component {
  render() {
    return (
      <div className="my-2">
        <div
          id="SearchToggler"
          className="btn btn-secondary btn-block rounded-0"
        >
          Vyhledávač
        </div>
        <UncontrolledCollapse toggler="#SearchToggler">
          <div
            style={{ width: "100%" }}
            className="bg-white border rounded-0 px-3 py-3"
          >
            <Row>
              <Col xs="6">
                <Label for="dodavatel">Dodavatel</Label>
                <Input placeholder="Dodavatel" name="dodavatel" />
                <Label for="urceno pro">Urceno pro</Label>
                <Input placeholder="meantfor" name="urceno pro" />
              </Col>
              <Col xs="6">
                <Label for="dodavatel">Odberatel</Label>
                <Input placeholder="Odberatel" name="dodavatel" />
                <Label for="urceno pro">Datum</Label>
                <Input type="date" placeholder="Datum" name="date" />
              </Col>
            </Row>
            <Row className="mt-4">
              <Col>
                <Button color="success" block>
                  Search
                </Button>
              </Col>
            </Row>
          </div>
        </UncontrolledCollapse>
      </div>
    );
  }
}

export default GeneralSearch;
