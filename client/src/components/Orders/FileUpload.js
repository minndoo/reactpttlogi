import React, { Component } from 'react';
import Dropzone from 'react-dropzone';
import { connect } from 'react-redux';

import { handleFiles } from '../../actions/OrderActions';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

class FileUpload extends Component {
  onDrop = files => {
    this.props.handleFiles(files);
  };
  render() {
    const { files } = this.props;
    return (
      <Dropzone
        style={{ flexGrow: 1 }}
        multiple
        onDrop={this.onDrop.bind(this)}
      >
        <div className="bg-white border rounded text-center pt-2">
          {files.length === 0 && (
            <div>
              <p>
                Pretahnete soubory do toho okna nebo na nej kliknete a vyberte
                soubory pro nahrani
              </p>
              <div className="px-2 py-2">
                <FontAwesomeIcon size="2x" icon="upload" />
              </div>
            </div>
          )}
          <ul>
            {files &&
              files.map((f, key) => {
                return <li key={key}>{f.name}</li>;
              })}
          </ul>
        </div>
      </Dropzone>
    );
  }
}

function mapStateToProps(state) {
  return { files: state.OrderReducer.order_files };
}

export default connect(
  mapStateToProps,
  { handleFiles },
)(FileUpload);
