import React, { Component } from 'react';
import { connect } from 'react-redux';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Link } from 'react-router-dom';
import { Table, Button, Row, UncontrolledCollapse } from 'reactstrap';
import { resetStock } from '../../actions/StockinoutActions';
import { toggleAuthorize } from '../../actions/loginActions';
class GeneralTable extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
    };
  }
  componentDidMount() {
    const { toggleAuthorize, token } = this.props;
    fetch('/orders', {
      method: 'GET',
      mode: 'cors',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        'Content-Length': '2000',
        Authorization: `Bearer ${token}`,
      },
    })
      .then(response => response.json())
      .then(data => {
        if (data.message !== 'Unauthorized') {
          return this.setState({
            data: data.result,
          });
        } else {
          return toggleAuthorize();
        }
      });
  }
  render() {
    const { data } = this.state;
    return (
      <div className="my-2">
        <div
          id="TableToggler"
          className="btn btn-secondary btn-block rounded-0"
        >
          Tabulka
        </div>
        <UncontrolledCollapse toggler="#TableToggler">
          <div className="px-3 bg-white">
            <Row>
              <Table size="sm" hover>
                <thead>
                  <tr>
                    <th>Číslo objednávky</th>
                    <th>Datum vytvoření</th>
                    <th>Poslední úprava</th>
                    <th>Typ</th>
                    <th />
                    <th />
                    <th />
                  </tr>
                </thead>
                <tbody>
                  {data.length !== 0 &&
                    data.map((order, index) => {
                      return (
                        <tr className="center-text" key={index}>
                          <td>
                            <Button
                              className="py-0"
                              color="link"
                              to={`/orders/${order._id}`}
                              tag={Link}
                            >
                              {order.order_number}
                            </Button>
                          </td>
                          <td>{order.delivery_date}</td>
                          <td>{order.delivery_date}</td>
                          <td>
                            {order.order_type === 'received'
                              ? 'Přijatá'
                              : 'Vystavená'}
                          </td>
                          <td>
                            <Button
                              color="link"
                              className="py-0"
                              onClick={this.props.resetStock}
                              to={`${
                                order.order_type === 'received'
                                  ? '/stockout'
                                  : '/stockin'
                              }/create/${order._id}`}
                              tag={Link}
                            >
                              <FontAwesomeIcon
                                className="text-primary"
                                icon="copy"
                              />
                            </Button>
                          </td>
                          <td>
                            <Button
                              color="link"
                              className="py-0"
                              to={`/orders/${order._id}/edit`}
                              tag={Link}
                            >
                              <FontAwesomeIcon
                                className="text-warning"
                                icon="edit"
                              />
                            </Button>
                          </td>
                          <td>
                            <Button color="link" className="py-0">
                              <FontAwesomeIcon
                                className="text-danger"
                                icon="trash"
                              />
                            </Button>
                          </td>
                        </tr>
                      );
                    })}
                </tbody>
              </Table>
            </Row>
          </div>
        </UncontrolledCollapse>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    router: state.router,
    token: state.token.token,
  };
}

export default connect(
  mapStateToProps,
  { resetStock, toggleAuthorize },
)(GeneralTable);
