import React, { Component } from 'react';
import { Row, Col, Button } from 'reactstrap';
import { Link } from 'react-router-dom';
import GeneralTable from './GeneralTable';
import GeneralSearch from './GeneralSearch';
import { connect } from 'react-redux';
import { clearForm } from '../../actions/OrderActions';

class General extends Component {
  render() {
    return (
      <div>
        <Row>
          <Col xs="6" className="px-0 py-0">
            <h2 className="float-left">Orders</h2>
          </Col>
          <Col xs="6" className="px-0 py-0">
            <Button
              color="success"
              className="float-right"
              to="/orders/create"
              onClick={this.props.clearForm}
              tag={Link}
            >
              Vytvořit objednávku
            </Button>
          </Col>
        </Row>
        <Row className="mt-2">
          <Col xs="12" className="px-0">
            <GeneralSearch />
          </Col>
        </Row>
        <Row className="mt-2">
          <Col xs="12" className="px-0">
            <GeneralTable />
          </Col>
        </Row>
      </div>
    );
  }
}

export default connect(
  null,
  { clearForm },
)(General);
