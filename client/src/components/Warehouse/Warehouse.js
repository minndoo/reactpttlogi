import React, { Component } from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

import General from './General';
import Edit from './Edit';

class Warehouse extends Component {
  render() {
    const { match, authorized } = this.props;
    if (!authorized) {
      return <Redirect to="/login" />;
    }
    return (
      <Switch>
        <Route path={match.url} exact component={General} />
        <Route path={`${match.url}/create`} component={Edit} />
        <Route path={`${match.url}/:warehouseId`} component={Edit} />
      </Switch>
    );
  }
}

const mapStateToProps = state => {
  return { authorized: state.token.authorized };
};

export default connect(mapStateToProps)(Warehouse);
