import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Col, Row, Input, Label, Button } from 'reactstrap';
import { connect } from 'react-redux';
import {
  handleInput,
  handleGetWarehouse,
} from '../../actions/WarehouseActions';
import { toggleAuthorize } from '../../actions/loginActions';

class Edit extends Component {
  postNewWarehouse() {
    const { match, warehouse, token } = this.props;
    let checkId = '';
    if (match.params.warehouseId) {
      checkId = '/' + match.params.warehouseId;
    }
    fetch(`/warehouse${checkId}`, {
      method: 'POST',
      mode: 'cors',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        'Content-Length': '2000',
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify(warehouse),
    });
  }
  componentDidMount() {
    const { token, toggleAuthorize, match, handleGetWarehouse } = this.props;
    if (match.params.warehouseId) {
      fetch(match.url, {
        method: 'GET',
        mode: 'cors',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          'Content-Length': '2000',
          Authorization: `Bearer ${token}`,
        },
      })
        .then(response => response.json())
        .then(data => {
          if (data.message === 'Unauthorized') {
            return toggleAuthorize();
          } else {
            return handleGetWarehouse(data.result);
          }
        });
    }
  }
  render() {
    const { warehouse, handleInput } = this.props;
    return (
      <div>
        <Row>
          <Col xs="12">
            <Label>Jmeno</Label>
            <Input
              name="CHANGE_WAREHOUSE_NAME"
              value={warehouse.name}
              onChange={handleInput}
            />
          </Col>
        </Row>
        <Row className="mt-3">
          <Col xs="4" />
          <Col xs="4">
            <Button
              block
              color="success"
              onClick={this.postNewWarehouse.bind(this)}
              to="/warehouse"
              tag={Link}
            >
              Vytvorit
            </Button>
          </Col>
          <Col xs="4" />
        </Row>
      </div>
    );
  }
}

const mapStateToPros = state => {
  return {
    warehouse: state.WarehouseReducer,
    token: state.token.token,
  };
};

export default connect(
  mapStateToPros,
  {
    handleInput,
    handleGetWarehouse,
    toggleAuthorize,
  },
)(Edit);
