import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { Col, Row, Table, Button } from 'reactstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { handleClearForm } from '../../actions/WarehouseActions';
import { toggleAuthorize } from '../../actions/loginActions';

class General extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
    };
    this.deleteWarehouse = this.deleteWarehouse.bind(this);
    this.updateWarehouse = this.updateWarehouse.bind(this);
  }
  deleteWarehouse(warehouse_id) {
    const { token, toggleAuthorize } = this.props;
    fetch(`/warehouse/${warehouse_id}`, {
      method: 'DELETE',
      mode: 'cors',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        'Content-Length': '2000',
        Authorization: `Bearer ${token}`,
      },
    })
      .then(res => res.json())
      .then(data => {
        if (data.message === 'Unauthorized') {
          toggleAuthorize();
        }
      });
  }
  updateWarehouse() {
    const { token, toggleAuthorize } = this.props;
    fetch('/warehouse', {
      method: 'GET',
      mode: 'cors',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        'Content-Length': '2000',
        Authorization: `Bearer ${token}`,
      },
    })
      .then(response => response.json())
      .then(data => {
        if (data.message !== 'Unauthorized') {
          return this.setState({ data: data.result });
        } else {
          return toggleAuthorize();
        }
      });
  }

  componentDidMount() {
    this.updateWarehouse();
  }

  render() {
    const { data } = this.state;
    return (
      <div>
        <Row>
          <Col xs="6" className="text-left">
            <h2>Sklady</h2>
          </Col>
          <Col xs="6" className="text-right">
            <Button
              color="success"
              to="/warehouse/create"
              tag={Link}
              onClick={this.props.handleClearForm}
            >
              Vytvořit sklad
            </Button>
          </Col>
        </Row>
        <Row className="mt-3">
          <Col xs="12">
            <Table size="sm" className="bg-white text-center">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Název</th>
                  <th>Počet skladovaných palet</th>
                  <th colSpan="2" />
                </tr>
              </thead>
              <tbody>
                {data.length !== 0 &&
                  data.map((warehouse, index) => {
                    return (
                      <tr key={index}>
                        <td>{index}</td>
                        <td>{warehouse.name}</td>
                        <td>{warehouse.storredPalletes}</td>

                        <td>
                          <Button
                            color="link"
                            className="py-0"
                            tag={Link}
                            to={`/warehouse/${warehouse._id}`}
                          >
                            <FontAwesomeIcon
                              icon="edit"
                              className="text-warning"
                            />
                          </Button>
                        </td>
                        <td>
                          <Button
                            color="link"
                            className="py-0"
                            onClick={() => {
                              this.deleteWarehouse(warehouse._id);
                              this.updateWarehouse();
                            }}
                            to="/warehouse"
                            tag={Link}
                          >
                            <FontAwesomeIcon
                              icon="trash"
                              className="text-danger"
                            />
                          </Button>
                        </td>
                      </tr>
                    );
                  })}
              </tbody>
            </Table>
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return { token: state.token.token };
};

export default connect(
  mapStateToProps,
  { toggleAuthorize, handleClearForm },
)(General);
