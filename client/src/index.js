import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';

import { createBrowserHistory } from 'history';
import { Route, Switch } from 'react-router-dom';
import { PersistGate } from 'redux-persist/integration/react';
import { ConnectedRouter } from 'connected-react-router';

import 'bootstrap/dist/css/bootstrap.min.css';
import './index.css';
import reduxStore from './store';
import App from './App';
import Login from './components/Login/Login';

const history = createBrowserHistory();

ReactDOM.render(
  <div>
    <Provider store={reduxStore.store}>
      <PersistGate loading={null} persistor={reduxStore.persistor}>
        <ConnectedRouter history={history}>
          <Switch>
            <Route path="/login" component={Login} />
            <Route path="/" component={App} />
          </Switch>
        </ConnectedRouter>
      </PersistGate>
    </Provider>
  </div>,
  document.getElementById('root'),
);
