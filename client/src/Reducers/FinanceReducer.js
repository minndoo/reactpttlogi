export default function reducer(state = {}, action) {
	switch (action.type) {
		case "FETCH_DATA_FROM_FINANCE": {
			return { ...state, ...action.value };
		}
		default: {
			return state;
		}
	}
}
