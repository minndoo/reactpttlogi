export default function reducer(
  state = {
    username: '',
    usersurname: '',
    userlogin: '',
    userpassword: '',
    confirmpassword: '',
    userRoles: [],
  },
  action,
) {
  switch (action.type) {
    case 'ADD_ROLE': {
      const newRoles = state.userRoles;
      newRoles.push(action.value);
      return { ...state, userRoles: newRoles };
    }
    case 'REMOVE_ROLE': {
      const index = state.userRoles.findIndex(role => role === action.value);

      const newArray = state.userRoles
        .slice(0, index)
        .concat(state.userRoles.slice(index + 1, state.userRoles.length));
      return { ...state, userRoles: newArray };
    }
    case 'CHANGE_USER_NAME': {
      return { ...state, username: action.value };
    }
    case 'CHANGE_USER_SURNAME': {
      return { ...state, usersurname: action.value };
    }
    case 'CHANGE_USER_LOGIN': {
      return { ...state, userlogin: action.value };
    }
    case 'CHANGE_USER_PASSWORD': {
      return { ...state, userpassword: action.value };
    }
    case 'CHANGE_USER_PASSWORD_CONFIRM': {
      return { ...state, confirmpassword: action.value };
    }
    case 'FETCH_USER': {
      return { ...state, ...action.value };
    }
    case 'CLEAR_USER_FORM': {
      return {
        ...state,
        username: '',
        usersurname: '',
        userlogin: '',
        userpassword: '',
        confirmpassword: '',
        userRoles: [],
      };
    }
    default: {
      return state;
    }
  }
}
