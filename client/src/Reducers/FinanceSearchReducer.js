export default function reducer(
	state = {
		search_supplier: "",
		search_date: "",
	},
	action,
) {
	switch (action.type) {
		case "CHANGE_SEACH_DATE": {
			return { ...state, search_supplier: action.value };
		}
		case "CHANGE_SEACH_SUPPLIER": {
			return { ...state, search_date: action.value };
		}
		default: {
			return state;
		}
	}
}
