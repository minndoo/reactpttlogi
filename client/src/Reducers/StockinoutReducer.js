export default function reducer(
  state = {
    _id: '',
    order_id: '',
    stockinout_number: '', //uniqid generator
    stockinout_type: '',
    realBoxes: 0,
    realPalletes: 0,
    palletesDiff: 0,
    boxesDiff: 0,
    delay_delivery_date: null,
  },
  action,
) {
  switch (action.type) {
    case 'STOCKINOUT_NUMBER_CHANGE': {
      return { ...state, stockinout_number: action.value };
    }
    case 'STOCKINOUT_TYPE_CHANGE': {
      return { ...state, stockinout_type: action.value };
    }
    case 'REAL_PALLET_CHANGE': {
      return { ...state, palletes: action.value };
    }
    case 'REAL_BOX_CHANGE': {
      return { ...state, boxes: action.value };
    }
    case 'DIFF_PALLET_CHANGE': {
      return { ...state, palletesDiff: action.value };
    }
    case 'DIFF_BOX_CHANGE': {
      return { ...state, boxesDiff: action.value };
    }
    case 'DELAY_DELIVERY_DATE_CHANGE': {
      return { ...state, delivery_date: action.value };
    }
    case 'SET_ORDER_ID_VALUE': {
      return { ...state, order_id: action.value };
    }
    case 'RESET_STOCKINOUT': {
      return {
        ...state,
        _id: '',
        stockinout_number: '', //uniqid generator
        stockinout_type: '',
        realBoxes: 0,
        realPalletes: 0,
        palletesDiff: 0,
        boxesDiff: 0,
        delay_delivery_date: null,
      };
    }
    default: {
      return state;
    }
  }
}
