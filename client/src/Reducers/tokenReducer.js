export default function reducer(
  state = {
    userlogin: '',
    userpassword: '',
    token: '',
    userPermissions: {},
    authorized: false,
  },
  action,
) {
  switch (action.type) {
    case 'CHANGE_USER_LOGIN': {
      return { ...state, userlogin: action.value };
    }
    case 'CHANGE_USER_PASSWORD': {
      return { ...state, userpassword: action.value };
    }
    case 'SET_TOKEN': {
      return { ...state, token: action.value };
    }
    case 'SET_PERMISSIONS': {
      return { ...state, permissions: action.value };
    }
    case 'TOGGLE_AUTHORIZE': {
      return { ...state, authorized: !state.authorized };
    }
    default: {
      return state;
    }
  }
}
