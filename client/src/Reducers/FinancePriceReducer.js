export default function reducer(
  state = {
    daily_price: 0,
    accept_price: 0,
    away_price: 0,
    toggle_price: false,
  },
  action,
) {
  switch (action.type) {
    case 'CHANGE_DAILY_PRICE': {
      return { ...state, daily_price: action.value };
    }
    case 'CHANGE_ACCEPT_PRICE': {
      return { ...state, accept_price: action.value };
    }
    case 'CHANGE_AWAY_PRICE': {
      return { ...state, away_price: action.value };
    }
    case 'TOGGLE_PRICE_EDIT': {
      return { ...state, toggle_price: !state.toggle_price };
    }
    case 'FETCH_DATA_FROM_FINANCE_PRICE': {
      return { ...state, ...action.value };
    }
    default: {
      return state;
    }
  }
}
