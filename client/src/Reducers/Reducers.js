import { combineReducers } from 'redux';
import UserReducer from './UserReducer';
import DOMReducer from './DOMReducer';
import OrderReducer from './OrderReducer';
import FinanceReducer from './FinanceReducer';
import FinanceSearchReducer from './FinanceSearchReducer';
import FinancePriceReducer from './FinancePriceReducer';
import StockinoutReducer from './StockinoutReducer';
import RolesReducer from './RolesReducer';
import WarehouseReducer from './WarehouseReducer';
import SPinformationReducer from './SPinformationReducer';
import token from './tokenReducer';

export default combineReducers({
  UserReducer,
  DOMReducer,
  OrderReducer,
  FinanceReducer,
  FinanceSearchReducer,
  FinancePriceReducer,
  StockinoutReducer,
  RolesReducer,
  WarehouseReducer,
  SPinformationReducer,
  token,
});
