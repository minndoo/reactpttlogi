export default function reducer(
  state = {
    _id: '',
    name: '',
    storredPalettes: 0,
  },
  action,
) {
  switch (action.type) {
    case 'CHANGE_WAREHOUSE_NAME': {
      return { ...state, name: action.value };
    }
    case 'CHANGE_STORRED_PALETTES': {
      return { ...state, storredPalettes: action.value };
    }
    case 'HANDLE_GET_WAREHOUSE': {
      return {
        ...state,
        ...action.value,
      };
    }
    case 'CLEAR_WAREHOUSE_FORM': {
      return { ...state, _id: '', name: '', storredPalettes: 0 };
    }
    default: {
      return state;
    }
  }
}
