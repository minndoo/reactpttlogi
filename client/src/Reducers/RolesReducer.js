export default function reducer(
  state = {
    rolename: '',
    permissions: {
      finance: {
        view: false,
        write: false,
        edit: false,
        delete: false,
        manage: false,
      },
      order: {
        view: false,
        write: false,
        edit: false,
        delete: false,
        manage: false,
      },
      spinformation: {
        view: false,
        write: false,
        edit: false,
        delete: false,
        manage: false,
      },
      stockin: {
        view: false,
        write: false,
        edit: false,
        delete: false,
        manage: false,
      },
      stockout: {
        view: false,
        write: false,
        edit: false,
        delete: false,
        manage: false,
      },
      user: {
        view: false,
        write: false,
        edit: false,
        delete: false,
        manage: false,
      },
      roles: {
        view: false,
        write: false,
        edit: false,
        delete: false,
        manage: false,
      },
    },
  },
  action,
) {
  switch (action.type) {
    case 'FINANCE_CHECK_VIEW': {
      return {
        ...state,
        permissions: {
          ...state.permissions,
          finance: { ...state.permissions.finance, view: action.value },
        },
      };
    }

    case 'FINANCE_CHECK_WRITE': {
      return {
        ...state,
        permissions: {
          ...state.permissions,
          finance: { ...state.permissions.finance, write: action.value },
        },
      };
    }

    case 'FINANCE_CHECK_EDIT': {
      return {
        ...state,
        permissions: {
          ...state.permissions,
          finance: { ...state.permissions.finance, edit: action.value },
        },
      };
    }
    case 'FINANCE_CHECK_DELETE': {
      return {
        ...state,
        permissions: {
          ...state.permissions,
          finance: { ...state.permissions.finance, delete: action.value },
        },
      };
    }

    case 'FINANCE_CHECK_MANAGE': {
      return {
        ...state,
        permissions: {
          ...state.permissions,
          finance: { ...state.permissions.finance, manage: action.value },
        },
      };
    }
    case 'ORDER_CHECK_VIEW': {
      return {
        ...state,
        permissions: {
          ...state.permissions,
          order: { ...state.permissions.order, view: action.value },
        },
      };
    }

    case 'ORDER_CHECK_WRITE': {
      return {
        ...state,
        permissions: {
          ...state.permissions,
          order: { ...state.permissions.order, write: action.value },
        },
      };
    }

    case 'ORDER_CHECK_EDIT': {
      return {
        ...state,
        permissions: {
          ...state.permissions,
          order: { ...state.permissions.order, edit: action.value },
        },
      };
    }
    case 'ORDER_CHECK_DELETE': {
      return {
        ...state,
        permissions: {
          ...state.permissions,
          order: { ...state.permissions.order, delete: action.value },
        },
      };
    }

    case 'ORDER_CHECK_MANAGE': {
      return {
        ...state,
        permissions: {
          ...state.permissions,
          order: { ...state.permissions.order, manage: action.value },
        },
      };
    }
    case 'STOCKIN_CHECK_VIEW': {
      return {
        ...state,
        permissions: {
          ...state.permissions,
          stockin: { ...state.permissions.stockin, view: action.value },
        },
      };
    }

    case 'STOCKIN_CHECK_WRITE': {
      return {
        ...state,
        permissions: {
          ...state.permissions,
          stockin: { ...state.permissions.stockin, write: action.value },
        },
      };
    }

    case 'STOCKIN_CHECK_EDIT': {
      return {
        ...state,
        permissions: {
          ...state.permissions,
          stockin: { ...state.permissions.stockin, edit: action.value },
        },
      };
    }
    case 'STOCKIN_CHECK_DELETE': {
      return {
        ...state,
        permissions: {
          ...state.permissions,
          stockin: { ...state.permissions.stockin, delete: action.value },
        },
      };
    }

    case 'STOCKIN_CHECK_MANAGE': {
      return {
        ...state,
        permissions: {
          ...state.permissions,
          stockin: { ...state.permissions.stockin, manage: action.value },
        },
      };
    }
    case 'STOCKOUT_CHECK_VIEW': {
      return {
        ...state,
        permissions: {
          ...state.permissions,
          stockout: { ...state.permissions.stockout, view: action.value },
        },
      };
    }

    case 'STOCKOUT_CHECK_WRITE': {
      return {
        ...state,
        permissions: {
          ...state.permissions,
          stockout: { ...state.permissions.stockout, write: action.value },
        },
      };
    }

    case 'STOCKOUT_CHECK_EDIT': {
      return {
        ...state,
        permissions: {
          ...state.permissions,
          stockout: { ...state.permissions.stockout, edit: action.value },
        },
      };
    }
    case 'STOCKOUT_CHECK_DELETE': {
      return {
        ...state,
        permissions: {
          ...state.permissions,
          stockout: { ...state.permissions.stockout, delete: action.value },
        },
      };
    }

    case 'STOCKOUT_CHECK_MANAGE': {
      return {
        ...state,
        permissions: {
          ...state.permissions,
          stockout: { ...state.permissions.stockout, manage: action.value },
        },
      };
    }
    case 'SPINFORMATION_CHECK_VIEW': {
      return {
        ...state,
        permissions: {
          ...state.permissions,
          spinformation: {
            ...state.permissions.spinformation,
            view: action.value,
          },
        },
      };
    }

    case 'SPINFORMATION_CHECK_WRITE': {
      return {
        ...state,
        permissions: {
          ...state.permissions,
          spinformation: {
            ...state.permissions.spinformation,
            write: action.value,
          },
        },
      };
    }

    case 'SPINFORMATION_CHECK_EDIT': {
      return {
        ...state,
        permissions: {
          ...state.permissions,
          spinformation: {
            ...state.permissions.spinformation,
            edit: action.value,
          },
        },
      };
    }
    case 'SPINFORMATION_CHECK_DELETE': {
      return {
        ...state,
        permissions: {
          ...state.permissions,
          finspinformationance: {
            ...state.permissions.spinformation,
            delete: action.value,
          },
        },
      };
    }

    case 'SPINFORMATION_CHECK_MANAGE': {
      return {
        ...state,
        permissions: {
          ...state.permissions,
          spinformation: {
            ...state.permissions.spinformation,
            manage: action.value,
          },
        },
      };
    }
    case 'USERS_CHECK_VIEW': {
      return {
        ...state,
        permissions: {
          ...state.permissions,
          user: { ...state.permissions.user, view: action.value },
        },
      };
    }

    case 'USERS_CHECK_WRITE': {
      return {
        ...state,
        permissions: {
          ...state.permissions,
          user: { ...state.permissions.user, write: action.value },
        },
      };
    }

    case 'USERS_CHECK_EDIT': {
      return {
        ...state,
        permissions: {
          ...state.permissions,
          user: { ...state.permissions.user, edit: action.value },
        },
      };
    }
    case 'USERS_CHECK_DELETE': {
      return {
        ...state,
        permissions: {
          ...state.permissions,
          user: { ...state.permissions.user, delete: action.value },
        },
      };
    }

    case 'USERS_CHECK_MANAGE': {
      return {
        ...state,
        permissions: {
          ...state.permissions,
          user: { ...state.permissions.user, manage: action.value },
        },
      };
    }
    case 'ROLES_CHECK_VIEW': {
      return {
        ...state,
        permissions: {
          ...state.permissions,
          roles: { ...state.permissions.roles, view: action.value },
        },
      };
    }

    case 'ROLES_CHECK_WRITE': {
      return {
        ...state,
        permissions: {
          ...state.permissions,
          roles: { ...state.permissions.roles, write: action.value },
        },
      };
    }

    case 'ROLES_CHECK_EDIT': {
      return {
        ...state,
        permissions: {
          ...state.permissions,
          roles: { ...state.permissions.roles, edit: action.value },
        },
      };
    }
    case 'ROLES_CHECK_DELETE': {
      return {
        ...state,
        permissions: {
          ...state.permissions,
          roles: { ...state.permissions.roles, delete: action.value },
        },
      };
    }

    case 'ROLES_CHECK_MANAGE': {
      return {
        ...state,
        permissions: {
          ...state.permissions,
          roles: { ...state.permissions.roles, manage: action.value },
        },
      };
    }
    case 'CHANGE_ROLE_NAME': {
      return { ...state, rolename: action.value };
    }
    case 'FETCH_EXISTING_ROLE': {
      return { ...state, ...action.value };
    }
    case 'CLEAR_FORM_ROLE': {
      return {
        ...state,
        rolename: '',
        permissions: {
          finance: {
            view: false,
            write: false,
            edit: false,
            delete: false,
            manage: false,
          },
          order: {
            view: false,
            write: false,
            edit: false,
            delete: false,
            manage: false,
          },
          spinformation: {
            view: false,
            write: false,
            edit: false,
            delete: false,
            manage: false,
          },
          stockin: {
            view: false,
            write: false,
            edit: false,
            delete: false,
            manage: false,
          },
          stockout: {
            view: false,
            write: false,
            edit: false,
            delete: false,
            manage: false,
          },
          user: {
            view: false,
            write: false,
            edit: false,
            delete: false,
            manage: false,
          },
          roles: {
            view: false,
            write: false,
            edit: false,
            delete: false,
            manage: false,
          },
        },
      };
    }
    default: {
      return state;
    }
  }
}
