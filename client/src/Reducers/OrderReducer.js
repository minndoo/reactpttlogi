export default function reducer(
  state = {
    _id: '',
    identification_number: '',
    order_number: '', //uniqid
    order_type: 'received',
    order_files: [],
    addressee: '',
    supplier: '',
    subscriber: '',
    description: '',
    meant_for: '',
    boxes: 0,
    palletes: 0,
    source_document: '',
    delivery_date: '',
  },
  action,
) {
  switch (action.type) {
    case 'IDENTIFICATION_NUMBER_CHANGE': {
      return { ...state, identification_number: action.value };
    }
    case 'ORDER_NUMBER_CHANGE': {
      return { ...state, order_number: action.value };
    }
    case 'DESCRIPTION_CHANGE': {
      return { ...state, description: action.value };
    }
    case 'ORDER_TYPE_CHANGE': {
      return { ...state, order_type: action.value };
    }
    case 'SUP_CHANGE': {
      return { ...state, supplier: action.value, subscriber: '' };
    }
    case 'SUB_CHANGE': {
      return { ...state, subscriber: action.value, supplier: '' };
    }
    case 'ADDRESSEE_CHANGE': {
      return { ...state, addressee: action.value };
    }
    case 'FILES_CHANGE': {
      return { ...state, order_files: action.files };
    }
    case 'PALLET_CHANGE': {
      return { ...state, palletes: action.value };
    }
    case 'BOX_CHANGE': {
      return { ...state, boxes: action.value };
    }
    case 'MEANT_FOR_CHANGE': {
      return { ...state, meant_for: action.value };
    }
    case 'DATE_CHANGE': {
      return { ...state, delivery_date: action.value };
    }
    case 'SOURCE_DOCUMENT_CHANGE': {
      return { ...state, source_document: action.value };
    }
    case 'HADNLE_FETCH_DATA_ORDER_ONE': {
      return { ...state, ...action.value };
    }
    case 'CLEAR_ORDER_FORM': {
      return {
        ...state,
        _id: '',
        identification_number: '',
        order_number: '', //uniqid
        order_type: 'received',
        order_files: [],
        addressee: '',
        supplier: '',
        subscriber: '',
        description: '',
        meant_for: '',
        boxes: 0,
        palletes: 0,
        source_document: '',
        delivery_date: '',
      };
    }
    default: {
      return state;
    }
  }
}
