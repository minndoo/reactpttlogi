export default function reducer(
  state = {
    search_supplier: "",
    search_subscriber: "",
    meant_for: "",
    date: ""
  },
  action
) {
  switch (action.type) {
    case "CHANGE_SEACH_SUBSCRIBER": {
      return { ...state, search_subscriber: action.value };
    }
    case "CHANGE_SEARCH_SUPPLIER": {
      return { ...state, search_supplier: action.value };
    }
    case "CHANGE_SEARCH_DATE": {
      return { ...state, date: action.value };
    }
    case "CHANGE_SEARCH_MEANT_FOR": {
      return { ...state, meant_for: action.value };
    }
    default: {
      return state;
    }
  }
}
