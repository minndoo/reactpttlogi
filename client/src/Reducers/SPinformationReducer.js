export default function reducer(
  state = {
    pallet_accept: 0,
    warehouse_accept: '',
    pallet_away: 0,
    warehouse_away: '',
  },
  action,
) {
  switch (action.type) {
    case 'TRANSACTION_PALLET_ACCEPT': {
      return { ...state, pallet_accept: action.value };
    }
    case 'TRANSACTION_PALLET_AWAY': {
      return { ...state, pallet_away: action.value };
    }
    case 'TRANSACTION_WAREHOUSE_ACCEPT': {
      return { ...state, warehouse_accept: action.value };
    }
    case 'TRANSACTION_WAREHOUSE_AWAY': {
      return { ...state, warehouse_away: action.value };
    }
    case 'CLEAR_SPI_FORM': {
      return { ...state, pallet_accept: 0, pallet_away: 0 };
    }
    case 'SET_WAREHOUSES': {
      return {
        ...state,
        warehouse_accept: action.value,
        warehouse_away: action.value,
      };
    }
    default: {
      return state;
    }
  }
}
