export function handleInput(e) {
  return { type: e.target.name, value: e.target.value };
}
export function addUserRole(roleToAdd) {
  return { type: 'ADD_ROLE', value: roleToAdd };
}
export function removeUserRole(roleToRemove) {
  return { type: 'REMOVE_ROLE', value: roleToRemove };
}

export function handleFetchUser(data) {
  const newUser = {
    userlogin: data.userlogin,
    userpassword: data.userpassword,
    confirmpassword: data.userpassword,
    username: data.username,
    usersurname: data.usersurname,
    roles: data.roles,
  };
  return { type: 'FETCH_USER', value: newUser };
}

export function clearUserForm() {
  return { type: 'CLEAR_USER_FORM' };
}
