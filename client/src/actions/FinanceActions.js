// action that handles input values
export function handleInput(e) {
  return { type: e.target.name, value: e.target.value };
}

export function togglePrice() {
  return { type: 'TOGGLE_PRICE_EDIT' };
}

export function handleFetchFinancePrice(data) {
  const dataNeeded = {
    daily_price: data.daily_price,
    away_price: data.away_price,
    accept_price: data.accept_price,
  };
  return { type: 'FETCH_DATA_FROM_FINANCE_PRICE', value: dataNeeded };
}
export function handleFetchDataFinance(data) {
  let neededData = {};
  if (data) {
    neededData = {
      date: data.date,
      pallet_accept: data.pallet_accept,
      pallet_away: data.pallet_away,
      pallet_stored: data.pallet_stored,
      accept_sum: data.accept_sum,
      away_sum: data.away_sum,
      daily_sum: data.away_sum,
    };
  }
  return { type: 'FETCH_DATA_FROM_FINANCE', value: neededData };
}
