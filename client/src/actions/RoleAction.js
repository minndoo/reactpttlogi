export function handleInput(e) {
  return { type: e.target.name, value: e.target.value };
}

export function handleCheck(e) {
  return { type: e.target.name, value: e.target.checked };
}

export function fetchExistingRole(data) {
  const existingRole = {
    rolename: data.rolename,
    permissions: data.permissions,
  };
  console.log(data, 'data role');
  return { type: 'FETCH_EXISTING_ROLE', value: existingRole };
}

export function clearFormRole() {
  return { type: 'CLEAR_FORM_ROLE' };
}
