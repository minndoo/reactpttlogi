export function handleInput(e) {
  return { type: e.target.name, value: e.target.value };
}

export function handleClearForm() {
  return { type: 'CLEAR_WAREHOUSE_FORM' };
}

export function calculateStorred(value) {
  return { type: 'CHANGE_STORRED_PALETTES', value: value };
}

export function handleGetWarehouse(data) {
  const getWarehouse = {
    _id: data._id,
    name: data.name,
    storredPalletes: data.storredPalletes,
  };
  return { type: 'HANDLE_GET_WAREHOUSE', value: getWarehouse };
}
