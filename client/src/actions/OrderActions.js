// action that handles file uploads
export function handleFiles(files) {
  return { type: 'FILES_CHANGE', files: files };
}

// action that handles input values
export function handleInput(e) {
  return { type: e.target.name, value: e.target.value };
}

export function handleInputGenerated(order_number) {
  return { type: 'ORDER_NUMBER_CHANGE', value: order_number };
}

export function handleFetchDataOrderOne(data) {
  let neededData;
  if (data) {
    neededData = {
      _id: data._id,
      identification_number: data.identification_number,
      order_number: data.order_number,
      order_type: data.order_type,
      order_files: data.order_files,
      addressee: data.addressee,
      supplier: data.supplier,
      subscriber: data.subscriber,
      description: data.description,
      meant_for: data.meant_for,
      boxes: data.boxes,
      palletes: data.palletes,
      source_document: data.source_document,
      delivery_date: data.delivery_date,
    };
  }
  return { type: 'HADNLE_FETCH_DATA_ORDER_ONE', value: neededData };
}

export function clearForm() {
  return { type: 'CLEAR_ORDER_FORM' };
}
