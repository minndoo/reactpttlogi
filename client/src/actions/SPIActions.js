export function handleInput(e) {
  return { type: e.target.name, value: e.target.value };
}

export function clearForm() {
  return { type: 'CLEAR_SPI_FORM' };
}

export function setWarehouses(warehouse) {
  return { type: 'SET_WAREHOUSES', value: warehouse };
}
