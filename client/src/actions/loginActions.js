export function handleInput(e) {
  return { type: e.target.name, value: e.target.value };
}

export function setToken(token) {
  return { type: 'SET_TOKEN', value: token };
}

export function setPermissions(permissions) {
  return { type: 'SET_PERMISSIONS', value: permissions };
}

export function toggleAuthorize() {
  return { type: 'TOGGLE_AUTHORIZE' };
}
