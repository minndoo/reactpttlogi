export function handleInput(e) {
  return { type: e.target.name, value: e.target.value };
}

export function calculatePalletes(val1, val2) {
  return { type: 'DIFF_PALLET_CHANGE', value: val1 - val2 };
}

export function calculateBoxes(val1, val2) {
  return { type: 'DIFF_BOX_CHANGE', value: val1 - val2 };
}

export function setOrderId(orderId) {
  return { type: 'SET_ORDER_ID_VALUE', value: orderId };
}

export function setStockinoutType(value) {
  return { type: 'STOCKINOUT_TYPE_CHANGE', value: value };
}

export function resetStock() {
  return { type: 'RESET_STOCKINOUT' };
}
