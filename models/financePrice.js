const mongoose = require("mongoose");

const priceSchema = mongoose.Schema({
	_id: mongoose.Schema.Types.ObjectId,
	id: Number,
	daily_price: Number,
	accept_price: Number,
	away_price: Number,
});

module.exports = mongoose.model("FinancePrice", priceSchema);
