const mongoose = require('mongoose');

const orderSchema = new mongoose.Schema({
  _id: mongoose.Schema.Types.ObjectId,
  identification_number: String,
  order_number: String,
  order_type: { type: String, default: 'received' },
  order_files: String,
  addressee: String,
  supplier: String,
  subscriber: String,
  description: String,
  meant_for: String,
  boxes: Number,
  palletes: Number,
  source_document: String,
  delivery_date: String,
});

module.exports = mongoose.model('Order', orderSchema);
