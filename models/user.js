const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
  _id: mongoose.Schema.Types.ObjectId,
  username: String,
  usersurname: String,
  userlogin: { type: String, index: true },
  userpassword: String,
  roles: Array,
});

module.exports = mongoose.model('User', userSchema);
