const mongoose = require('mongoose');

const roleSchema = new mongoose.Schema({
  _id: mongoose.Schema.Types.ObjectId,
  rolename: { type: String, index: true },
  permissions: {
    type: Object,
    finance: {
      view: { type: Boolean, default: false },
      write: { type: Boolean, default: false },
      edit: { type: Boolean, default: false },
      delete: { type: Boolean, default: false },
      manage: { type: Boolean, default: false },
    },
    order: {
      view: { type: Boolean, default: false },
      write: { type: Boolean, default: false },
      edit: { type: Boolean, default: false },
      delete: { type: Boolean, default: false },
      manage: { type: Boolean, default: false },
    },
    spinformation: {
      view: { type: Boolean, default: false },
      write: { type: Boolean, default: false },
      edit: { type: Boolean, default: false },
      delete: { type: Boolean, default: false },
      manage: { type: Boolean, default: false },
    },
    stockin: {
      view: { type: Boolean, default: false },
      write: { type: Boolean, default: false },
      edit: { type: Boolean, default: false },
      delete: { type: Boolean, default: false },
      manage: { type: Boolean, default: false },
    },
    stockout: {
      view: { type: Boolean, default: false },
      write: { type: Boolean, default: false },
      edit: { type: Boolean, default: false },
      delete: { type: Boolean, default: false },
      manage: { type: Boolean, default: false },
    },
    user: {
      view: { type: Boolean, default: false },
      write: { type: Boolean, default: false },
      edit: { type: Boolean, default: false },
      delete: { type: Boolean, default: false },
      manage: { type: Boolean, default: false },
    },
    roles: {
      view: { type: Boolean, default: false },
      write: { type: Boolean, default: false },
      edit: { type: Boolean, default: false },
      delete: { type: Boolean, default: false },
      manage: { type: Boolean, default: false },
    },
  },
});

module.exports = mongoose.model('Role', roleSchema);
