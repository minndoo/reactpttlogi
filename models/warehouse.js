const mongoose = require('mongoose');

const warehouseSchema = new mongoose.Schema({
  _id: mongoose.Schema.Types.ObjectId,
  name: String,
  storredPalletes: { type: Number, default: 0 },
});

module.exports = mongoose.model('Warehouse', warehouseSchema);
