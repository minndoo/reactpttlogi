const mongoose = require("mongoose");

const financeSchema = new mongoose.Schema({
	_id: mongoose.Schema.Types.ObjectId,
	pallet_accept: { type: Number, default: 0 },
	pallet_away: { type: Number, default: 0 },
	pallet_stored: { type: Number, default: 0 },
	date: { type: Date, default: Date.now() },
	accept_sum: { type: Number, default: 0 },
	away_sum: { type: Number, default: 0 },
	daily_sum: { type: Number, default: 0 },
});

module.exports = mongoose.model("Finance", financeSchema);
