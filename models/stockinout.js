const mongoose = require('mongoose');

const stockinoutSchema = new mongoose.Schema({
  _id: mongoose.Schema.Types.ObjectId,
  order_id: String,
  realBoxes: Number,
  realPalletes: Number,
  palletesDiff: Number,
  boxesDiff: Number,
  stockinout_type: String,
  delay_delivery_date: String,
});

module.exports = mongoose.model('Stockinout', stockinoutSchema);
