const mongoose = require('mongoose');

const SPInformationSchema = new mongoose.Schema({
  _id: mongoose.Schema.Types.ObjectId,
  date: String,
  boxesReceived: Number,
  boxesAway: Number,
  palletReceived: Number,
  palletAway: Number,
  warehouse: String,
});

module.exports = mongoose.model('SPInformation', SPInformationSchema);
