const cron = require('node-cron');
const Warehouse = require('../models/warehouse');
const Finance = require('../models/finance');
const FinancePrice = require('../models/financePrice');
const mongoose = require('mongoose');

module.exports = () => {
  cron.schedule('59 59 23 * * *', async () => {
    const warehouses = await Warehouse.find()
      .then(result => result)
      .catch(error => error);
    const prices = await FinancePrice.findOne()
      .then(result => result)
      .catch(error => error);
    let calculatedPalettes = 0;
    let calculatedPrice = 0;

    warehouses.map(warehouse => {
      if (warehouse.storredPalletes < 0) {
        return;
      }
      calculatedPalettes += warehouse.storredPalletes;
    });
    calculatedPrice = prices.daily_price * calculatedPalettes;

    const newTransaction = new Finance({
      _id: mongoose.Types.ObjectId(),
      pallet_accept: 0,
      pallet_away: 0,
      pallet_stored: calculatedPalettes,
      date: Date.now(),
      accept_sum: 0,
      away_sum: 0,
      daily_sum: calculatedPrice,
    });

    newTransaction
      .save()
      .then(result => result)
      .catch(error => error);
  });
};
