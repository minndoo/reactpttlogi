const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');

const Stockinout = require('../models/stockinout');

router.get('/', (req, res) => {
  Stockinout.find({ stockinout_type: 'stockin' })
    .then(result => res.status(200).json({ result: result }))
    .catch(error => res.status(500).json({ error: error }));
});

router.get('/:stockinId', (req, res) => {
  Stockinout.find({ _id: req.params._id })
    .then(result => res.status(200).json({ result: result }))
    .catch(error => res.status(500).json({ error: error }));
});

router.post('/', (req, res) => {
  const requestBody = req.body;
  console.log(req.body);
  const stockinoutReceipt = new Stockinout({
    _id: mongoose.Types.ObjectId(),
    order_id: requestBody.order_id,
    realBoxes: requestBody.realBoxes,
    realPalletes: requestBody.realPalletes,
    palletesDiff: requestBody.palletesDiff,
    boxesDiff: requestBody.boxesDiff,
    stockinout_type: requestBody.stockinout_type,
    delay_delivery_date: requestBody.delivery_date,
  });

  stockinoutReceipt
    .save()
    .then(result => res.status(201).json({ result: result }))
    .catch(error => res.status(500).json({ error: error }));
});

router.post('/:stockinId', (req, res) => {
  Stockinout.findOne({ _id: req.params.stockinId })
    .then(result => res.status(200).json({ result: result }))
    .catch(error => res.status(500).json({ error: error }));
});

module.exports = router;
