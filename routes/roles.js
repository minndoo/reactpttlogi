const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');

const Role = require('../models/role');

router.get('/', (req, res) => {
  Role.find()
    .then(result => res.status(200).json({ result: result }))
    .catch(error => res.status(500).json({ error: error }));
});

router.get('/:roleId', (req, res) => {
  Role.findOne({ _id: req.params.roleId })
    .then(result => res.status(200).json({ result: result }))
    .catch(error => res.status(500).json({ error: error }));
});

router.post('/', (req, res) => {
  const reqBody = req.body;
  const newRole = new Role({
    _id: mongoose.Types.ObjectId(),
    rolename: reqBody.rolename,
    permissions: reqBody.permissions,
  });
  newRole
    .save()
    .then(result => res.status(201).json({ result: result }))
    .catch(error => res.status(500).json({ error: error }));
});

router.post('/:roleId', (req, res) => {
  const reqBody = req.body;
  Role.updateOne(
    { _id: req.params.roleId },
    {
      $set: {
        rolename: reqBody.rolename,
        permissions: reqBody.permissions,
      },
    },
  )
    .then(result => res.status(200).json({ result: result }))
    .catch(error => res.status(500).json({ error: error }));
});

router.delete('/:roleId', (req, res) => {
  Role.deleteOne({ _id: req.params.roleId })
    .then(result => res.status(200).json({ result: result }))
    .catch(error => res.status(500).json({ error: error }));
});

module.exports = router;
