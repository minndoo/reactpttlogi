const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const Warehouse = require('../models/warehouse');
const SPIinformation = require('../models/spinformation');

router.get('/', (req, res) => {
  SPIinformation.find()
    .then(result => res.status(200).json({ result: result }))
    .catch(error => res.status(500).json({ error: error }));
});

router.post('/', async (req, res) => {
  const reqBody = req.body;
  const newSPItransaction = new SPIinformation({
    _id: mongoose.Types.ObjectId(),
    date: reqBody.date,
    boxesReceived: reqBody.boxesReceived,
    boxesAway: reqBody.boxesAway,
    palletReceived: reqBody.palletReceived,
    palletAway: reqBody.palletAway,
    warehouse: reqBody.warehouse,
  });
  newSPItransaction
    .save()
    .then(result => res.status(201).json({ result: result }))
    .catch(error => res.status(500).json({ error: error }));
  let updateStore = await Warehouse.findOne({ name: reqBody.warehouse })
    .then(result => result)
    .catch(error => error);
  Warehouse.updateOne(
    { name: reqBody.warehouse },
    {
      $set: {
        storredPalettes:
          updateStore.storredPalettes +
          reqBody.palletReceived -
          reqBody.palletAway,
      },
    },
  );
});

router.delete('/', (req, res) => {
  SPIinformation.delete({ _id: req.body._id })
    .then(() => res.status(204))
    .catch(error => res.status(500).json({ error: error }));
});

module.exports = router;
