const express = require("express");
const mongoose = require("mongoose");
const router = express.Router();

const FinancePrice = require("../models/financePrice");

router.get("/", (req, res) => {
	FinancePrice.findOne()
		.then(result => res.status(200).json({ result: result }))
		.catch(error => res.status(500).json({ error: error }));
});

router.post("/", (req, res, next) => {
	res.status(200).json({
		page: "finance post",
	});
	FinancePrice.updateOne(
		{ id: 1 },
		{
			$set: {
				daily_price: req.body.daily_price,
				accept_price: req.body.accept_price,
				away_price: req.body.away_price,
			},
		},
	).then(response => console.log(response));
	next();
});

module.exports = router;
