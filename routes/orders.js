const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');

const Order = require('../models/order');

router.get('/', (req, res) => {
  Order.find({})
    .then(result => res.status(200).json({ result: result }))
    .catch(error => res.status(500).json({ error: error }));
});

router.get('/:orderId', (req, res) => {
  Order.findOne({ _id: req.params.orderId })
    .then(result => res.status(200).json({ result: result }))
    .catch(error => res.status(500).json({ error: error }));
});

router.post('/', (req, res) => {
  const newProduct = new Order({
    _id: mongoose.Types.ObjectId(),
    identification_number: req.body.identification_number,
    order_number: req.body.order_number,
    order_type: req.body.order_type,
    order_files: req.body.order_files,
    addressee: req.body.addressee,
    supplier: req.body.supplier,
    subscriber: req.body.subscriber,
    description: req.body.description,
    meant_for: req.body.meant_for,
    boxes: req.body.boxes,
    palletes: req.body.palletes,
    source_document: req.body.source_document,
    delivery_date: req.body.delivery_date,
  });
  newProduct
    .save()
    .then(result => res.status(201).json({ result: result }))
    .catch(error => res.status(500).json({ error: error }));
});

router.post('/:orderId', (req, res) => {
  Order.updateOne(
    {
      _id: req.params.orderId,
    },
    {
      $set: {
        identification_number: req.body.identification_number,
        order_number: req.body.order_number,
        order_type: req.body.order_type,
        order_files: req.body.order_files,
        addressee: req.body.addressee,
        supplier: req.body.supplier,
        subscriber: req.body.subscriber,
        description: req.body.description,
        meant_for: req.body.meant_for,
        boxes: req.body.boxes,
        palletes: req.body.palletes,
        source_document: req.body.source_document,
        delivery_date: req.body.delivery_date,
      },
    },
  )
    .then(() => {
      res.status(204);
    })
    .catch(error => res.stataus(500).json({ error: error }));
});

router.delete('/:orderId', (req, res) => {
  Order.delete({ _id: req.params.orderId })
    .then(result =>
      res.status(200).json({
        result: result,
        message: 'Order deleted successfully',
      }),
    )
    .catch(error => res.status(500).json({ error: error }));
});

module.exports = router;
