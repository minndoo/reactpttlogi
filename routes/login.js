const express = require('express');
const mongoose = require('mongoose');
const User = require('../models/user');
const router = express.Router();
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

router.post('/', (req, res) => {
  User.find({ userlogin: req.body.userlogin })
    .then(result => {
      if (result.length < 1) {
        return res.status(401).json({
          message: 'Unauthorized',
        });
      }
      bcrypt
        .compare(req.body.userpassword, result[0].userpassword)
        .then(tokenResult => {
          if (tokenResult) {
            const token = jwt.sign(
              {
                userlogin: result[0].userlogin,
                userId: result[0]._id,
              },
              process.env.JWT_KEY,
              {
                expiresIn: '1h',
              },
            );
            return res.status(200).json({
              token: token,
            });
          }
        });
    })
    .catch(error => {
      return res.status(500).json({
        error: error,
      });
    });
});

module.exports = router;
