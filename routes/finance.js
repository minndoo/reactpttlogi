const express = require('express');
const mongoose = require('mongoose');
const router = express.Router();

const Finance = require('../models/finance');

router.get('/', (req, res) => {
  Finance.find()
    .then(result => res.status(200).json({ result: result }))
    .catch(err => res.status(404).json({ error: error.message }));
});

router.post('/', (req, res, next) => {
  const newTransaction = new Finance({
    _id: mongoose.Types.ObjectId(),
    date: req.body.date,
    pallet_accept: req.body.pallet_accept,
    pallet_away: req.body.pallet_away,
    pallet_stored: req.body.pallet_stored,
    accept_sum: req.body.accept_sum,
    away_sum: req.body.away_sum,
    daily_sum: req.body.daily_sum,
  });
  newTransaction
    .save()
    .then(() => res.status(201).json({ result: result }))
    .catch(error =>
      res.status(500).json({
        message: "Couldn't add transaction to the resource",
        error: error,
      }),
    );

  next();
});

module.exports = router;
