const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const Warehouse = require('../models/warehouse');

router.get('/', (req, res) => {
  Warehouse.find()
    .then(result => res.status(200).json({ result: result }))
    .catch(error => res.status(500).json({ error: error }));
});

router.post('/', (req, res) => {
  console.log(req.body);
  console.log('get body');
  const wareStore = new Warehouse({
    _id: mongoose.Types.ObjectId(),
    name: req.body.name,
    storredPalletes: 0,
  });
  wareStore
    .save()
    .then(result => {
      res.status(201).json({ result: result });
    })
    .catch(error => {
      res.status(500).json({ error: error });
    });
});

router.get('/:warehouseId', (req, res) => {
  Warehouse.findOne({ _id: req.params.warehouseId })
    .then(result => res.status(200).json({ result: result }))
    .catch(error => res.status(500).json({ error: error }));
});

router.post('/:warehouseId', (req, res) => {
  Warehouse.updateOne(
    { _id: req.params.warehouseId },
    {
      $set: {
        name: req.body.name,
        storedPalletes: req.body.storedPalletes,
      },
    },
  )
    .then(result => res.status(200).json({ result: result }))
    .catch(error => res.status(500).json({ error: error }));
});

router.delete('/:warehouseId', (req, res) => {
  Warehouse.deleteOne({ _id: req.params.warehouseId })
    .then(result => res.status(200).json({ result: result }))
    .catch(error => res.status(500).json({ error: error }));
});

module.exports = router;
