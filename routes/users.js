const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const User = require('../models/user');

router.get('/', (req, res) => {
  User.find()
    .then(result => res.status(200).json({ result: result }))
    .catch(error => res.status(500).json({ error: error }));
});

router.get('/:userId', (req, res) => {
  User.findOne({ _id: req.params.userId })
    .then(result => res.status(200).json({ result: result }))
    .catch(error => res.status(500).json({ error: error }));
});

router.post('/', (req, res) => {
  //password crypt needed
  User.find({ userlogin: req.body.userlogin }).then(result => {
    if (result.length >= 1) {
      return res.status(409).json({
        message: 'This login name already exists',
      });
    } else {
      bcrypt.hash(reqBody.userpassword, 10, (err, hash) => {
        if (err) {
          return res.status(500).json({
            err,
          });
        } else {
          const newUser = new User({
            _id: mongoose.Types.ObjectId(),
            username: reqBody.username,
            usersurname: reqBody.usersurname,
            userlogin: reqBody.userlogin,
            userpassword: hash,
            roles: reqBody.userRoles,
          });
          console.log(newUser.userpassword);
          newUser
            .save()
            .then(result => res.status(201).json({ result }))
            .catch(error => res.status(500).json({ error }));
        }
      });
    }
  });
  const reqBody = req.body;
});

router.post('/:userId', (req, res) => {
  const reqBody = req.body;
  User.updateOne(
    { _id: req.params.userId },
    {
      $set: {
        username: reqBody.username,
        userLogin: reqBody.userLogin,
        userPassword: reqBody.userPassword,
      },
    },
  )
    .then(result => res.status(200).json({ result: result }))
    .catch(error => res.status(500).json({ error: error }));
});

router.delete('/:userId', (req, res) => {
  User.deleteOne({ _id: req.params.userId })
    .then(result => res.status(204).json({ result }))
    .catch(error => res.status(500).json({ error }));
});

//delete

module.exports = router;
